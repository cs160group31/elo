package com.eclecticeverywhere.elo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Presentation {
    private String name;
    private ArrayList<Section> sections;
    private double timeAllowed;
    private ArrayList<Double> warnings;
    private ArrayList<AnalyticPair> practiceRuns;
    public int mCurrentSlide;
    public int totalSlides;

    public Presentation(String name) {
        this.name = name;
        sections = new ArrayList<Section>();
        warnings = new ArrayList<Double>();
        practiceRuns = new ArrayList<AnalyticPair>();
    }

    public String getName() {
        return name;
    }

    public void addSection(Section s) {
        sections.add(s);
    }

    public void setTime(double time) {
        timeAllowed = time;
    }

    public double getTimeAllowed() {
        return timeAllowed;
    }

    public void dropSection(int i) {
        if (i < sections.size()) {
            sections.remove(i);
        }
    }

    public AnalyticPair getLatestRun() {
        if (practiceRuns.isEmpty()) {
            return null;
        }
        return practiceRuns.get(0);
    }

    public void addPracticeRun(AnalyticPair ap) {
        practiceRuns.add(0, ap);
    }

    public ArrayList<AnalyticPair> getPracticeRuns() {
        return practiceRuns;
    }

    public ArrayList<Section> getSections() {
        return sections;
    }

    public void addWarning(double warning) {
        warnings.add(warning);
    }

    public void removeWarning(double warning) {
        warnings.remove(warning);
    }

    public ArrayList<Double> getWarnings() {
        return warnings;
    }

    public ArrayList<Double> getSectionTimes() {
        ArrayList<Double> retVal = new ArrayList<Double>();
        for (Section s : sections) {
            retVal.add(s.getTimeAllowed());
        }
        return retVal;
    }

    public JSONObject serialize() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("timeAllowed", timeAllowed);
            JSONArray warningArr = new JSONArray(warnings);
            obj.put("warnings", warningArr);
            ArrayList<JSONObject> sectionArr = new ArrayList<JSONObject>();
            for (Section s : sections) {
                sectionArr.add(s.serialize());
            }
            obj.put("sections", sectionArr);
        } catch (JSONException e) {

        }
        return obj;

    }



}