package com.eclecticeverywhere.elo;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.MenuItem;

/**
 * Created by Eric on 12/4/15.
 */
public class PresentationControllerActivity extends SingleFragmentActivity {
    @Override
    protected Fragment createFragment() {
        return new PresentationControllerFragment();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Log.i("TEST", "back button pressed from pres control frag");

                FragmentManager fm = getSupportFragmentManager();
                PresentationControllerFragment fragment = (PresentationControllerFragment)
                        fm.findFragmentById(R.id.fragment_container);
//                if (PresentationControllerFragment.sStarted == false) {
                    fragment.stopWatch();
//                }
//                finish();
                Intent intent = new Intent(this, PresentationTabActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
