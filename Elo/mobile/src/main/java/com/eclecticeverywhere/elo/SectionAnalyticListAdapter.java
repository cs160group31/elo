package com.eclecticeverywhere.elo;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import android.widget.RelativeLayout.LayoutParams;

public class SectionAnalyticListAdapter extends BaseAdapter {
    Context context;
    ArrayList<AnalyticPair> data;
    private static LayoutInflater inflater = null;
    public SectionAnalyticListAdapter(Context context, ArrayList<AnalyticPair> data) {
        // TODO Auto-generated constructor stub
        Log.d("MAIN", "created sla");
        this.context = context;
        this.data = data;
        Log.d("MAIN", String.format("data size %d", this.data.size()));
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return data.size() + 1;
    }

    @Override
    public AnalyticPair getItem(int position) {
        // TODO Auto-generated method stub
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;
        SectionAnalyticViewHolder savh;
        if (vi == null) {
            vi = inflater.inflate(R.layout.section_analytics_row, parent, false);

            TextView dateText = (TextView) vi.findViewById(R.id.analytic_date);
            TextView presTime = (TextView) vi.findViewById(R.id.analytic_time);
            LinearLayout timerBar = (LinearLayout) vi.findViewById(R.id.time_bar);
            savh = new SectionAnalyticViewHolder();
            savh.dateText = dateText;
            savh.presTime = presTime;
            savh.timerBar = timerBar;
            vi.setTag(savh);
        } else {
            savh = (SectionAnalyticViewHolder)vi.getTag();
        }
            if(savh.timerBar.getChildCount() > 0)
                savh.timerBar.removeAllViews();
            savh.dateText.setTypeface(Typefaces.COOPER_HEWITT_MEDIUM);
            savh.presTime.setTypeface(Typefaces.COOPER_HEWITT_MEDIUM);
            double expTime = PresentationManager.getInstance().activeSection.getTimeAllowed();

            if (position == 0) {
                savh.dateText.setText("EXPECTED");
                int expMin = (int) expTime;
                int expSec = Math.round((float) ((expTime - expMin) * 60));
                savh.presTime.setText(String.format("%d:%d%d", expMin, expSec / 10, expSec % 10));
                TextView t = new TextView(context);
                t.setLayoutParams(new ViewGroup.LayoutParams(
                        200,
                        ViewGroup.LayoutParams.FILL_PARENT));
                t.setBackgroundColor(ContextCompat.getColor(context, R.color.dark_gray));
                savh.timerBar.addView(t);
                return vi;
            }
            TextView t = new TextView(context);
            AnalyticPair ap = data.get(position - 1);
            double pixels = ap.time / (2 * expTime) * 400;
            if (position == 1) {
                savh.dateText.setText("LATEST");
                vi.setBackgroundColor(ContextCompat.getColor(context, R.color.glacier));
                t.setBackgroundColor(ContextCompat.getColor(context, R.color.tealesque));
            } else {
                savh.dateText.setText(ap.dateToString());
                t.setBackgroundColor(ContextCompat.getColor(context, R.color.medium_gray));
            }
            int curMin = (int) ap.time;
            int curSec = Math.round((float) (ap.time - curMin) * 60);
            savh.presTime.setText(String.format("%d:%d%d", curMin, curSec / 10, curSec % 10));
            // t.setHeight(60);
            //t.setWidth((int) pixels);
            t.setLayoutParams(new ViewGroup.LayoutParams(
                    (int) pixels,
                    ViewGroup.LayoutParams.FILL_PARENT));
            savh.timerBar.addView(t);



        return vi;
    }

    static class SectionAnalyticViewHolder {
        TextView dateText;
        TextView presTime;
        LinearLayout timerBar;
    }


}