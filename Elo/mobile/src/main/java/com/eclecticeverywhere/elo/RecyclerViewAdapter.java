package com.eclecticeverywhere.elo;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * Created by winstonkim on 12/6/15.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RowViewHolder> {
    Context context;
    ArrayList<RowItems> itemsList;

    public RecyclerViewAdapter(Context context, ArrayList<RowItems> itemsList) {
        this.context = context;
        this.itemsList = itemsList;
    }

    @Override
    public int getItemCount() {
        if (itemsList == null) {
            return 0;
        } else {
            return itemsList.size();
        }
    }

    @Override
    public RowViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.analytics_table_row, viewGroup, false);
        RowViewHolder viewHolder = new RowViewHolder(view);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(RowViewHolder rowViewHolder, int position) {
        RowItems items = itemsList.get(position);
        rowViewHolder.number.setText(String.valueOf(items.getNumber()));
        rowViewHolder.expected.setText(String.valueOf(items.getExpected()));
        rowViewHolder.actual.setText(String.valueOf(items.getActual()));
    }

    public RowItems getItem(int position) {
        return itemsList.get(position);
    }
}