package com.eclecticeverywhere.elo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Wearable;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONObject;
import org.w3c.dom.Text;

import java.net.URISyntaxException;
import java.util.Date;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by Eric on 12/3/15.
 */
public class PresentationControllerFragment extends Fragment implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        MessageApi.MessageListener
{

    private final static String TAG = "TEST";
    private final static String TAG2 = "DATA";

    private static GoogleApiClient sGoogleApiClient;
    private static int instances = 0;

    private static DataSender sDataSender;
    private static MessageSender sMessageSender;

    private boolean connected;
    private static boolean sClientConnected = false;

    private boolean mAlreadyLoaded = false;
    public static boolean sStarted;

    private static int sCurrentSlide = 1;

    private Socket mSocket;
    private String baseURL =
            "https://docs.google.com/presentation/d/1Nyq55kHxvM17mbUfkFrKey_Yb2kQelc-53XMPLQ6_3Y/" +
                    "embed?start=false&loop=false&delayms=900000&slide=";

    private TextView mMessageTextView;
    private ProgressBar mProgressBar;
    private Button mEndPresentationButton;
    private Button mStartPresentationButton;


    private boolean phoneConnected;
    private boolean websiteConnected;

    private Presentation mPresentation;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPresentation = PresentationManager.getInstance().activePresentation;
        Log.i(TAG2, "name: " + mPresentation.getName());
        Log.i(TAG2, "time allowed: " + mPresentation.getTimeAllowed());
        Log.i(TAG2, "warnings: " + mPresentation.getWarnings());
        Log.i(TAG2, "section times: " + mPresentation.getSectionTimes());

        Log.i(TAG, "started presenter controller fragment");

        if (sGoogleApiClient == null) {
            initGoogleApiClient();
        } else {
            initSocket();
        }
    }

    protected void stopWatch() {
        sMessageSender.sendMessage(Constants.MESSAGE_PATH, Constants.MESSAGE_STOP);
    }

    private void initSocket() {
        // Connect to Server
        try {
            // IP of Elo Web View server [see Constants].
            mSocket = IO.socket(Constants.SERVER_URL);
//            mSocket = IO.socket("http://10.142.37.218:5000/controller");
        } catch (URISyntaxException e) {
            Log.i(TAG, "something went wrong!");
        }

        mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.i(TAG, "things connected!");
            }
        });

        mSocket.on(Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.i(TAG, "connection error: " + args[0].toString());
            }
        });

        mSocket.on("connected", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                String msg = args[0].toString();
                Log.i(TAG, "phone: " + phoneConnected + " web: " + websiteConnected);
                Log.i(TAG, "something connected: " + msg);
                if (msg.equals("phone")) {
                    phoneConnected = true;
                    Log.i(TAG, "phone connected");
                } else if (msg.equals("website")) {
                    websiteConnected = true;
                    Log.i(TAG, "website connected");
                }

                if (phoneConnected && websiteConnected) {
                    sendMetaData();
                }
            }
        });

        mSocket.on("slides loaded", new Emitter.Listener() {
            @Override
            public void call(Object... args) {

                JsonParser parser = new JsonParser();
                JsonObject o = parser.parse(args[0].toString()).getAsJsonObject();

                int slideCount = o.get("slideCount").getAsInt();
                int currentSlide = o.get("currentSlide").getAsInt();

                Log.i(TAG, "loaded slides!");
                Log.i(TAG, "slide count: " + slideCount + " currentSlide: " + currentSlide);

                if (sStarted) {
                    sMessageSender.sendMessage(
                            Constants.MESSAGE_PATH, Constants.MESSAGE_RELOADED);
                } else {
                    sMessageSender.sendMessage(Constants.MESSAGE_PATH,
                            Constants.MESSAGE_LOADED);
                }

                if (getActivity() != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mProgressBar.setVisibility(View.GONE);
                            if (sStarted) {
                                mEndPresentationButton.setVisibility(View.VISIBLE);
                                mStartPresentationButton.setVisibility(View.GONE);
                                mMessageTextView.setText(getActivity()
                                        .getString(R.string.presentation_loaded_message));
                            } else {
                                mEndPresentationButton.setVisibility(View.GONE);
                                mStartPresentationButton.setVisibility(View.VISIBLE);
                                mMessageTextView.setText(getActivity()
                                        .getString(R.string.ready_message));

                            }
                        }
                    });
                }

            }
        });

        mSocket.connect();
    }


    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        Log.i(TAG, "new message received!asdfasfasfasdfadfasfd!!!!");
        String messagePath = messageEvent.getPath();

        if (messagePath.equals(Constants.MESSAGE_ANALYTICS_PATH)) {
            final String json = new String(messageEvent.getData());

            Gson gson = new Gson();
            AnalyticsData data = gson.fromJson(json, AnalyticsData.class);

            AnalyticPair dataPair = new AnalyticPair(new Date(), data.totalTime, data.peekCount);
            PresentationManager.getInstance().activePresentation.addPracticeRun(dataPair);

            Log.i(TAG, "message analytics received");
            getActivity().finish();
            sStarted = false;
            Intent intent = new Intent(getActivity(), PresentationTabActivity.class);
            intent.setAction(Constants.ACTION_START_ACTIVITIES);
            getActivity().startActivity(intent);
            getActivity().overridePendingTransition
                    (R.anim.slide_in_right, R.anim.slide_out_right);
        }

        if (messagePath.equals(Constants.MESSAGE_PATH)) {
            final String message = new String(messageEvent.getData());
            Log.i(TAG, "message: " + message);
            if (message.equals(Constants.MESSAGE_FORWARD)) {
                Log.i(TAG, "message forward received");
                sCurrentSlide += 1;
                mSocket.emit("change slide", "next");
            } else if (message.equals(Constants.MESSAGE_BACK)) {
                Log.i(TAG, "message back received");
                sCurrentSlide -= 1;
                mSocket.emit("change slide", "prev");
            } else if (message.equals(Constants.MESSAGE_ANALYTICS)) {
                Log.i(TAG, "message analytics received");
                getActivity().finish();
                sStarted = false;
                Intent intent = new Intent(getActivity(), PresentationTabActivity.class);
                intent.setAction(Constants.ACTION_START_ACTIVITIES);
                getActivity().startActivity(intent);
                getActivity().overridePendingTransition
                        (R.anim.slide_in_right, R.anim.slide_out_right);
            } else if (message.equals(Constants.MESSAGE_STARTED)) {
                sStarted = true;
                if (getActivity() != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mProgressBar.setVisibility(View.GONE);
                            mEndPresentationButton.setVisibility(View.VISIBLE);
                            mStartPresentationButton.setVisibility(View.INVISIBLE);
                            mMessageTextView.setText(getActivity()
                                    .getString(R.string.presentation_loaded_message));
                        }
                    });
                }
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_pres_controller, container, false);

        mMessageTextView = (TextView) v.findViewById(R.id.pres_controller_loading_message);
        mProgressBar = (ProgressBar) v.findViewById(R.id.loading_progress_bar);
        mEndPresentationButton = (Button) v.findViewById(R.id.end_presentation_button);
        mStartPresentationButton = (Button) v.findViewById(R.id.start_presentation_button);

        mMessageTextView.setTypeface(Typefaces.COOPER_HEWITT_BOLD);
        mEndPresentationButton.setTypeface(Typefaces.COOPER_HEWITT_BOLD);

        mEndPresentationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sStarted = false;
                stopWatch();
                getActivity().finish();
                Intent intent = new Intent(getActivity(), PresentationTabActivity.class);
                intent.setAction(Constants.ACTION_START_ACTIVITIES);
                startActivity(intent);
                getActivity().overridePendingTransition
                        (R.anim.slide_in_right, R.anim.slide_out_right);
            }
        });

        mStartPresentationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sStarted = true;
                Log.i(TAG, "started!");
                sMessageSender.sendMessage(Constants.MESSAGE_PATH, Constants.MESSAGE_STARTED);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mMessageTextView.setText(getActivity()
                                .getString(R.string.presentation_loaded_message));
                        mProgressBar.setVisibility(View.GONE);
                        mEndPresentationButton.setVisibility(View.VISIBLE);
                        mStartPresentationButton.setVisibility(View.GONE);
                    }
                });
            }
        });

        return v;
    }




    private void initGoogleApiClient() {
        Log.i(TAG, "init google api client");
        sGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        sGoogleApiClient.connect();
    }

    @Override
    public void onResume() {
        super.onResume();

        Log.i(TAG, "on resume");
        Log.i(TAG, "phone: " + phoneConnected + " web: " + websiteConnected);
        Log.i(TAG, "Started : " + sStarted);

        if (sStarted) {
            sMessageSender.sendMessage(
                    Constants.MESSAGE_PATH, Constants.MESSAGE_RELOADED);
        }

        if (mSocket != null) {

            mSocket.connect();
            if (phoneConnected && websiteConnected) {
                sendMetaData();
            }
        }

        if (sClientConnected) {
            if (instances == 0) {
                Wearable.MessageApi.addListener(sGoogleApiClient, this);
                instances += 1;
            }
            Log.i(TAG, "" + instances);
            if (sMessageSender == null) {
                sMessageSender = new MessageSender(sGoogleApiClient);
            }

            if (sStarted) {
                sMessageSender.sendMessage(Constants.MESSAGE_PATH, Constants.MESSAGE_RELOADED);
            } else {
                startWatch();
            }
        }

    }

    private class SlidesMeta {
        private String mUrl;
        private int mNumSlides;
        private int mCurrentSlide;

        public SlidesMeta(String url, int numSlides, int currentSlide) {
            mUrl = url;
            mNumSlides = numSlides;
            mCurrentSlide = currentSlide;
        }
    }

    private void sendMetaData() {
        SlidesMeta meta = new SlidesMeta(baseURL, mPresentation.totalSlides, sCurrentSlide);
        Gson gson = new Gson();
        String json = gson.toJson(meta);
        mSocket.emit("slides meta", json);
        if (!sStarted) {
            sMessageSender.sendMessage(Constants.MESSAGE_PATH,
                    Constants.MESSAGE_CONNECTED);
            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mMessageTextView.setText(getActivity()
                                .getString(R.string.loading_message));
                    }
                });
            }
        }

    }

    public void onPause() {
        super.onPause();
        Log.i(TAG, "in pause, started: " + sStarted);
        Log.i(TAG, "on pause");
        mAlreadyLoaded = false;
        if (sClientConnected) {
            if (instances == 1) {
                Wearable.MessageApi.removeListener(sGoogleApiClient, this);
                instances -= 1;
            }
            Log.i(TAG, "" + instances);
        }

        connected = false;
        mSocket.disconnect();

    }

    /**
     * METHODS FOR IMPLEMENTING GoogleApiClient INTERFACES!
     **/
    @Override
    public void onConnected(Bundle connectionHint) {
        sClientConnected = true;
        if (instances == 0) {
            Wearable.MessageApi.addListener(sGoogleApiClient, this);
            instances += 1;
        }

        Log.i(TAG, "on connected - " + instances);

        initSocket();
        if (sMessageSender == null)
            sMessageSender = new MessageSender(sGoogleApiClient);
        sDataSender = new DataSender(sGoogleApiClient);
        // start watch
        startWatch();
    }
    @Override
    public void onConnectionSuspended(int cause) {
//        Wearable.MessageApi.removeListener(sGoogleApiClient, this);
        sClientConnected = false;
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) { }

    public void startWatch() {
        sMessageSender.sendMessage(Constants.MESSAGE_PATH, Constants.MESSAGE_START);
        Gson gson = new Gson();
        String json = gson.toJson(mPresentation);
        Log.i("DATA", "data message sent!");
        sMessageSender.sendMessage(Constants.MESSAGE_DATA_PATH, json);
    }


    public class AnalyticsData {
        int peekCount;
        int totalTime;

        AnalyticsData(int peeks, int time) {
            peekCount = peeks;
            totalTime = time;
        }
    }
}
