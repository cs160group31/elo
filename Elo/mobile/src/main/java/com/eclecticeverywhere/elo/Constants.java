package com.eclecticeverywhere.elo;

import android.graphics.Color;

/**
 * Created by Eric on 12/4/15.
 */
public class Constants {
    public final static int HIGHLIGHT_COLOR = Color.parseColor("#3AA3B4");
    public final static int HIGHLIGHT_HEIGHT = 10;
    public final static String EXTRA_DATA_MAP = "com.bignerdranch.android.criminalintent.dataMap";
    public final static String TOOLBAR_TITLE_KEY = "TOOLBAR_TITLE";

    public final static String SERVER_URL = "https://eloviewer.herokuapp.com/controller";

    public final static String MESSAGE_PATH = "/message";
    public final static String MESSAGE_DATA_PATH = "/message_data";
    public final static String MESSAGE_ANALYTICS_PATH = "/message_analytics";
    public final static String MESSAGE_START = "/message/start";
    public final static String MESSAGE_CONNECTED = "/message/connected";
    public final static String MESSAGE_LOADED = "/message/loaded";
    public final static String MESSAGE_FORWARD = "/message/forward";
    public final static String MESSAGE_BACK = "/message/back";
    public final static String MESSAGE_ANALYTICS = "/message/analytics";
    public final static String MESSAGE_STOP = "/message/stop";
    public final static String MESSAGE_RELOADED = "/message/reloaded";
    public final static String MESSAGE_STARTED = "/message/started";

    public final static String ACTION_START_ACTIVITIES = "action:start_activities";

}
