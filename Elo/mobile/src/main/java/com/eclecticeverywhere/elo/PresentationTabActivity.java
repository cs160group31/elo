package com.eclecticeverywhere.elo;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Eric on 12/2/15.
 */
public class PresentationTabActivity extends TabActivity {
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        String action = intent.getAction();

        if (action != null && action.equals(Constants.ACTION_START_ACTIVITIES)) {
            mViewPager.setCurrentItem(2, false);
        }
    }

    @Override
    protected void initTabs() {
        mTabs = new Fragment[] {
                new SectionsFragment(),
                new TimersFragment(),
                new AnalyticsFragment()
        };

        mTabNames = new String[] {
                "SECTIONS",
                "TIMERS",
                "ANALYTICS"
        };
    }

    @Override
    public void onPageSelected(int position) {
        super.onPageSelected(position);

        if (position == 1) {
            mAddButton.setVisibility(View.VISIBLE);
        } else {
            mAddButton.setVisibility(View.INVISIBLE);
        }
    }
}
