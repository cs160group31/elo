package com.eclecticeverywhere.elo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.google.android.gms.wearable.DataMap;

/**
 * Created by Eric on 12/4/15.
 */
public class Utils {

    public static void startActivitySlideLeft(Activity activity, Class<?> cls, DataMap dataMap) {
        startActivitySlideUp(activity, cls, dataMap);
        activity.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    public static void startActivitySlideUp(Activity activity, Class<?> cls, DataMap dataMap) {
        Intent intent = new Intent(activity, cls);
        intent.putExtra(Constants.EXTRA_DATA_MAP, dataMap.toBundle());
        activity.startActivity(intent);
    }

}
