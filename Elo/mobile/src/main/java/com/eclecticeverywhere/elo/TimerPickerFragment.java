package com.eclecticeverywhere.elo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by Eric on 12/4/15.
 */
public class TimerPickerFragment extends DialogFragment {

    private TextView minutesBox;
    private TextView secondsBox;
    private ImageView minutesUp, minutesDown, secondsUp, secondsDown;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        View v = LayoutInflater.from(getActivity())
                .inflate(R.layout.fragment_timer_picker, null);

        AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setView(v)
                .create();

        TextView titleText = (TextView) v.findViewById(R.id.title_text_view);
        titleText.setTypeface(Typefaces.COOPER_HEWITT_MEDIUM);

        minutesBox = (TextView) v.findViewById(R.id.minutes_box);
        minutesBox.setTypeface(Typefaces.COOPER_HEWITT_MEDIUM);

        secondsBox = (TextView) v.findViewById(R.id.seconds_box);
        secondsBox.setTypeface(Typefaces.COOPER_HEWITT_MEDIUM);

        minutesUp = (ImageView) v.findViewById(R.id.minutes_up);
        minutesDown = (ImageView) v.findViewById(R.id.minutes_down);
        secondsUp = (ImageView) v.findViewById(R.id.seconds_up);
        secondsDown = (ImageView) v.findViewById(R.id.seconds_down);

        minutesUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int min = Integer.parseInt(minutesBox.getText().toString()) + 1;
                if (min < 100) {
                    String s = String.format("%d%d", min / 10, min % 10);
                    minutesBox.setText(s);
                }
            }
        });

        minutesDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int min = Integer.parseInt(minutesBox.getText().toString()) - 1;
                if (min >= 0) {
                    String s = String.format("%d%d", min / 10, min % 10);
                    minutesBox.setText(s);
                }
            }
        });

        secondsUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int sec = (Integer.parseInt(secondsBox.getText().toString()) + 15) % 60;
                String s = String.format("%d%d", sec / 10, sec % 10);
                secondsBox.setText(s);
            }
        });

        secondsDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int sec = (Integer.parseInt(secondsBox.getText().toString()) - 15) % 60;
                if (sec < 0) {
                    sec += 60;
                }
                String s = String.format("%d%d", sec / 10, sec % 10);
                secondsBox.setText(s);

            }
        });

        Button setButton = (Button) v.findViewById(R.id.timer_picker_set_button);
        setButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendResult(Activity.RESULT_OK);
            }
        });

        Button cancelButton = (Button) v.findViewById(R.id.timer_picker_cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendResult(Activity.RESULT_CANCELED);
            }
        });


        setButton.setTypeface(Typefaces.COOPER_HEWITT_BOLD);
        cancelButton.setTypeface(Typefaces.COOPER_HEWITT_BOLD);

        return dialog;
    }

    private void sendResult(int resultCode) {
        if (getTargetFragment() == null) {
            return;
        }
        Intent intent = new Intent();
        if (resultCode == Activity.RESULT_OK) {
            int m = Integer.parseInt(minutesBox.getText().toString());
            int s = Integer.parseInt(secondsBox.getText().toString());
            intent.putExtra("minutes", m);
            intent.putExtra("seconds", s);
        }
        // put extras here
        getTargetFragment()
                .onActivityResult(getTargetRequestCode(), resultCode, intent);
        dismiss();
    }

}
