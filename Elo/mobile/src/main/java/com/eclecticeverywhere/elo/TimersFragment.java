package com.eclecticeverywhere.elo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by Eric on 12/3/15.
 */
public class TimersFragment extends Fragment {

    private ImageButton mAddButton;
    private Presentation p;
    private ListView timerList;
    private TimersListAdapter tla;

    private static final int REQUEST_TIMER = 0;
    private static final String DIALOG_TIMER = "DialogTimer";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_timers, container, false);

        mAddButton = ((TabActivity) getActivity()).mAddButton;
        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("TEST", "Timer Picker started!");
                FragmentManager manager = getFragmentManager();
                TimerPickerFragment dialog = new TimerPickerFragment();
                dialog.setTargetFragment(TimersFragment.this, REQUEST_TIMER);
                dialog.show(manager, DIALOG_TIMER);
            }
        });
        p = PresentationManager.getInstance().activePresentation;
        timerList = (ListView)v.findViewById(R.id.timer_list);
        ArrayList<Double> warnings = p.getWarnings();
        tla = new TimersListAdapter(getContext(), warnings);
        timerList.setAdapter(tla);

        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            Log.d("MAIN", "timer set");
            int m = data.getIntExtra("minutes", 0);
            int s = data.getIntExtra("seconds", 0);
            double d = m + s / 60.0;
            if (d > 0) {
                tla.addItem(d);
                tla.notifyDataSetChanged();
                //tla = new TimersListAdapter(getContext(), p.getWarnings());
                //timerList.setAdapter(tla);
            }
            return;
        }

        if (requestCode == REQUEST_TIMER) {
            Log.i("TEST", "Timer Picker finished!");
        }
    }
}
