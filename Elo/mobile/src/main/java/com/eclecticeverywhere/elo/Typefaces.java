package com.eclecticeverywhere.elo;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Typeface;

/**
 * Created by Eric on 12/2/15.
 */
public class Typefaces {

    public static Typeface COOPER_HEWITT_MEDIUM;
    public static Typeface COOPER_HEWITT_SEMIBOLD;
    public static Typeface COOPER_HEWITT_BOLD;
    public static Typeface COOPER_HEWITT_HEAVY;

    public static void init(Context context) {
        AssetManager assets = context.getAssets();
        COOPER_HEWITT_MEDIUM =
                Typeface.createFromAsset(assets, "fonts/CooperHewitt-Medium.otf");
        COOPER_HEWITT_SEMIBOLD =
                Typeface.createFromAsset(assets, "fonts/CooperHewitt-Semibold.otf");
        COOPER_HEWITT_BOLD =
                Typeface.createFromAsset(assets, "fonts/CooperHewitt-Bold.otf");
        COOPER_HEWITT_HEAVY =
                Typeface.createFromAsset(assets, "fonts/CooperHewitt-Heavy.otf");
    }
}
