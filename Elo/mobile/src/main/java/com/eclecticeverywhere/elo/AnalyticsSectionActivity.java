package com.eclecticeverywhere.elo;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by winstonkim on 12/7/15.
 */
public class AnalyticsSectionActivity extends Activity {
    private RowItems row;
    private ListView analyticDisplay;
    private SectionAnalyticListAdapter sala;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.analytics_section);
        row = (RowItems)getIntent().getSerializableExtra("row");
        analyticDisplay = (ListView)findViewById(R.id.section_analytics_list);
        //ArrayList<AnalyticPair> dummy = new ArrayList<AnalyticPair>();
        //dummy.add(new AnalyticPair(new Date(2015, 11, 1), 6.683));
        //dummy.add(new AnalyticPair(new Date(2015, 10, 27), 6.5));
        //dummy.add(new AnalyticPair(new Date(2015, 10, 26), 7.6));
        //dummy.add(new AnalyticPair(new Date(2015, 10, 24), 8.1));
        sala = new SectionAnalyticListAdapter(this, PresentationManager.getInstance().activeSection.getPracticeRuns());
        analyticDisplay.setAdapter(sala);
        TextView sectionNum = (TextView)findViewById(R.id.sectionNum);
        sectionNum.setTypeface(Typefaces.COOPER_HEWITT_MEDIUM);
        sectionNum.setText(row.getNumber().toUpperCase() + " ANALYTICS");
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }
}