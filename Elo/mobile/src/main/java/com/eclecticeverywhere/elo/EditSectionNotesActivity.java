package com.eclecticeverywhere.elo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class EditSectionNotesActivity extends Activity {

    private EditText noteSpace;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_section_notes);
        Section s = (Section)getIntent().getSerializableExtra("section");
        TextView t = (TextView)findViewById(R.id.title_text_view);
        t.setTypeface(Typefaces.COOPER_HEWITT_MEDIUM);
        t.setText(s.getName());
        noteSpace = (EditText)findViewById(R.id.note_space);
        noteSpace.setTypeface(Typefaces.COOPER_HEWITT_MEDIUM);
        noteSpace.setText(s.getNotes());
        Button setButton = (Button) findViewById(R.id.note_picker_set_button);
        setButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                String newNotes = noteSpace.getText().toString();
                i.putExtra("newNotes", newNotes);
                setResult(Activity.RESULT_OK, i);
                finish();
            }
        });

        Button cancelButton = (Button) findViewById(R.id.note_picker_cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                setResult(Activity.RESULT_CANCELED, i);
                finish();
            }
        });
        setButton.setTypeface(Typefaces.COOPER_HEWITT_BOLD);
        cancelButton.setTypeface(Typefaces.COOPER_HEWITT_BOLD);
    }
}