package com.eclecticeverywhere.elo;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

public class SectionsListAdapter extends BaseAdapter {
    Context context;
    ArrayList<Section> data;
    private static LayoutInflater inflater = null;
    public SectionsListAdapter(Context context, ArrayList<Section> data) {
        // TODO Auto-generated constructor stub
        Log.d("MAIN", "created sla");
        this.context = context;
        this.data = data;
        Log.d("MAIN", String.format("data size %d", this.data.size()));
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public Section getItem(int position) {
        // TODO Auto-generated method stub
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;
        Section s = data.get(position);
        if (vi == null)
            vi = inflater.inflate(R.layout.sectionlist_row, parent, false);
        TextView secNum = (TextView) vi.findViewById(R.id.section_number);
        TextView name = (TextView) vi.findViewById(R.id.section_name);
        TextView bounds = (TextView) vi.findViewById(R.id.section_bounds);
        TextView time = (TextView) vi.findViewById(R.id.section_time);
        secNum.setTypeface(Typefaces.COOPER_HEWITT_MEDIUM);
        name.setTypeface(Typefaces.COOPER_HEWITT_MEDIUM);
        bounds.setTypeface(Typefaces.COOPER_HEWITT_MEDIUM);
        time.setTypeface(Typefaces.COOPER_HEWITT_MEDIUM);
        secNum.setText(String.format("%d", position + 1));
        int color = SectionsFragment.colorSeq[position % SectionsFragment.colorSeq.length];
        secNum.setBackgroundColor(ContextCompat.getColor(context, color));
        name.setText(s.getName());
        bounds.setText(String.format("%d-%d", s.getSectionStart(), s.getSectionEnd()));
        double d = s.getTimeAllowed();
        int min = (int)d;
        int sec = (int)((d - min) * 60);
        time.setText(String.format("%d:%d%d", min, sec/10, sec % 10));
        return vi;
    }
}