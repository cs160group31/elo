package com.eclecticeverywhere.elo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.wearable.DataMap;

import java.util.ArrayList;

/**
 * Created by Eric on 12/3/15.
 */
public class SectionsFragment extends Fragment {

    public final static int[] colorSeq = {R.color.section1, R.color.section2, R.color.section3, R.color.section4};
    private Button mEditSectionsButton;
    private ListView sectionsDisplay;
    private SectionsListAdapter sla;
    private TextView totalPresTime;
    private ArrayList<Section> sectionsList;
    private View v;
    private boolean firsttime = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_sections, container, false);

        mEditSectionsButton = (Button) v
                .findViewById(R.id.edit_sections_button);
        mEditSectionsButton.setTypeface(Typefaces.COOPER_HEWITT_BOLD);
        mEditSectionsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DataMap dataMap = new DataMap();
                dataMap.putString(Constants.TOOLBAR_TITLE_KEY, "SECTIONS");
                Utils.startActivitySlideUp(getActivity(), EditSectionsTabActivity.class, dataMap);
            }
        });
        sectionsDisplay = (ListView)v.findViewById(R.id.sections_display);
        sectionsDisplay.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Section s = sla.getItem(position);
                PresentationManager.getInstance().activeSection = s;
                Intent i = new Intent(getActivity(), SectionDetailsActivity.class);
                startActivity(i);
            }
        });
        totalPresTime = (TextView)v.findViewById(R.id.total_time);
        totalPresTime.setTypeface(Typefaces.COOPER_HEWITT_MEDIUM);
        sectionsList = PresentationManager.getInstance().activePresentation.getSections();
        setHeaderDisplay(sectionsList);
        sla = new SectionsListAdapter(getActivity(), sectionsList);
        sectionsDisplay.setAdapter(sla);

        return v;
    }

    @Override
    public void onResume(){
        if (!firsttime && sectionsList.size() > 0) {
            setHeaderDisplay(sectionsList);
            sla = new SectionsListAdapter(getActivity(), sectionsList);
            sectionsDisplay.setAdapter(sla);

        }
        if (firsttime){
            firsttime = false;
        }
        super.onResume();
    }

    public void setHeaderDisplay(ArrayList<Section> sections) {
        ArrayList<Integer> sizes = new ArrayList<Integer>();
        double totalTime = 0.0;
        int numSlides = 0;
        for (Section s : sections) {
            int size = s.getSectionEnd() - s.getSectionStart() + 1;
            sizes.add(size);
            numSlides += size;
            totalTime += s.getTimeAllowed();
        }
        if (totalTime > 0.0) {

            int min = (int) totalTime;
            int sec = (int) ((totalTime - min) * 60);
            totalPresTime.setText(String.format("%d:%d%d", min, sec / 10, sec % 10));
        } else {
            totalPresTime.setText("");
        }
        LinearLayout view = (LinearLayout) v.findViewById(R.id.section_length_bar);
        view.removeAllViews();
        for (int i = 0 ; i < sizes.size(); i++) {
            int pixels = (int)(sizes.get(i) / (double)numSlides * 600);
            TextView t = new TextView(getActivity());
            int color = colorSeq[i % colorSeq.length];
            t.setHeight(60);
            t.setWidth(pixels);
            t.setBackgroundColor(ContextCompat.getColor(getActivity(), color));
            view.addView(t);
        }


    }
}
