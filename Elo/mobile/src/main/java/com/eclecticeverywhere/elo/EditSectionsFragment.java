package com.eclecticeverywhere.elo;

import android.content.Intent;
import android.support.v4.app.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.wearable.DataMap;

import java.util.ArrayList;

/**
 * Created by Eric on 12/3/15.
 */
public class EditSectionsFragment extends Fragment {

    public final static int[] colorSeq = {R.color.section1, R.color.section2, R.color.section3, R.color.section4};
    private ImageButton mAddButton;
    private MultiSlider multiSlider;
    private ListView sectionsDisplay;
    private EditSectionsListAdapter sla;
    private TextView totalPresTime;
    private View v;
    private TextView sectionSlide;
    private ArrayList<Section> sectionsList;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_edit_sections, container, false);
        sectionSlide = (TextView) v.findViewById(R.id.section_slide);
//        final ArrayList<TextView> vals = new ArrayList<TextView>();
//        vals.add((TextView) v.findViewById(R.id.value1));
//        vals.add((TextView) v.findViewById(R.id.value2));
//        vals.add((TextView) v.findViewById(R.id.value3));
//        vals.add((TextView) v.findViewById(R.id.value4));
//        vals.add((TextView) v.findViewById(R.id.value5));
//        vals.add((TextView) v.findViewById(R.id.value6));
//        vals.add((TextView) v.findViewById(R.id.value7));

        multiSlider = (MultiSlider)v.findViewById(R.id.multiSlider);

        mAddButton = (ImageButton) v.findViewById(R.id.tab_add_image_button1);
        mAddButton.setVisibility(View.VISIBLE);
        mAddButton.setImageResource(R.drawable.plus_button);
        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                multiSlider.addThumb();
                sectionsList.add(new Section("New Section", 16, 16));
                ((EditSectionsTabActivity) getActivity()).setSection(sectionsList);
                ((EditSectionsTabActivity) getActivity()).SavePreferences();
                setHeaderDisplay(sectionsList);
                sla = new EditSectionsListAdapter(getActivity(), sectionsList);
                sectionsDisplay.setAdapter(sla);
                multiSlider.setVisibility(View.GONE);
                multiSlider.setVisibility(View.VISIBLE);

            }
        });
        multiSlider.setOnThumbValueChangeListener(new MultiSlider.SimpleOnThumbValueChangeListener() {
            @Override
            public void onValueChanged(MultiSlider multiSlider, MultiSlider.Thumb thumb, int thumbIndex, int value) {
                if (thumbIndex >= sectionsList.size()-1){
                    sectionsList.add(new Section("New Section", 16, 16));
                }
                else {
                    sectionsList.get(thumbIndex).setSectionEnd(value);
                    if(thumbIndex < sectionsList.size()-1){
                        sectionsList.get(thumbIndex + 1).setSectionStart(value + 1);
                    }
                    setHeaderDisplay(sectionsList);
                    sla = new EditSectionsListAdapter(getActivity(), sectionsList);
                    sectionsDisplay.setAdapter(sla);
                }
                ((EditSectionsTabActivity)getActivity()).SavePreferences();
                ((EditSectionsTabActivity)getActivity()).setSection(sectionsList);
            }

        });


        // sections

        sectionsDisplay = (ListView)v.findViewById(R.id.sections_display);
        totalPresTime = (TextView)v.findViewById(R.id.total_time);
        totalPresTime.setTypeface(Typefaces.COOPER_HEWITT_MEDIUM);
        sectionsList = PresentationManager.getInstance().activePresentation.getSections();
        ((EditSectionsTabActivity)getActivity()).SavePreferences();
        ((EditSectionsTabActivity)getActivity()).setSection(sectionsList);
        setHeaderDisplay(sectionsList);
        sla = new EditSectionsListAdapter(getActivity(), sectionsList);
        sectionsDisplay.setAdapter(sla);

        return v;
    }
    @Override
    public void onResume(){
        sectionsList = PresentationManager.getInstance().activePresentation.getSections();
        if(sectionsList!=null && sectionsList.size()!=0) {
            for (int i = sectionsList.size();i>3;i--){
                multiSlider.addThumb();
                multiSlider.setVisibility(View.GONE);
                multiSlider.setVisibility(View.VISIBLE);
            }
            setHeaderDisplay(sectionsList);
            sla = new EditSectionsListAdapter(getActivity(), sectionsList);
            sectionsDisplay.setAdapter(sla);
        }
        super.onResume();
    }
    public void setHeaderDisplay(ArrayList<Section> sections) {
        ArrayList<Integer> sizes = new ArrayList<Integer>();
        double totalTime = 0.0;
        int numSlides = 0;
        for (Section s : sections) {
            int size = s.getSectionEnd() - s.getSectionStart() + 1;
            sizes.add(size);
            numSlides += size;
            totalTime += s.getTimeAllowed();
        }
        int min = (int)totalTime;
        int sec = (int)((totalTime - min) * 60);
        totalPresTime.setText(String.format("%d:%d%d", min, sec / 10, sec % 10));
        LinearLayout view = (LinearLayout) v.findViewById(R.id.section_length_bar);
        for (int i = 0 ; i < sizes.size(); i++) {
            int pixels = (int)(sizes.get(i) / (double)numSlides * 600);
            TextView t = new TextView(getActivity());
            int color = colorSeq[i % colorSeq.length];
            t.setHeight(60);
            t.setWidth(pixels);
            t.setBackgroundColor(ContextCompat.getColor(getActivity(), color));
            view.addView(t);
        }


    }
}