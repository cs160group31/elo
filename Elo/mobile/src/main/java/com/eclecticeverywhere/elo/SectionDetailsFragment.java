package com.eclecticeverywhere.elo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by michaelling on 12/5/15.
 */
public class SectionDetailsFragment extends Fragment {

    private Section s;
    private ListView timerList;
    private ListView analyticsList;
    private TimersListAdapter tla;
    private SectionAnalyticListAdapter sala;
    private RelativeLayout notesView;
    private TextView notesBox;
    private ImageButton mAddButton;
    private View v;

    private static final int REQUEST_TIMER = 0;
    private static final String DIALOG_TIMER = "DialogTimer";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_section_details, container, false);
        s = PresentationManager.getInstance().activeSection;
        TextView t = (TextView)v.findViewById(R.id.title_text_view);
        notesView = (RelativeLayout)v.findViewById(R.id.notes_area);
        notesBox = (TextView)v.findViewById(R.id.notes_box);
        String notes = s.getNotes();
        if (notes == null) {
            notesBox.setText("ADD SECTION NOTES...");
        } else {
            notesBox.setText(s.getNotes());
        }
        mAddButton = (ImageButton)v.findViewById(R.id.tab_add_image_button);
        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("TEST", "Timer Picker started!");
                FragmentManager manager = getFragmentManager();
                TimerPickerFragment dialog = new TimerPickerFragment();
                dialog.setTargetFragment(SectionDetailsFragment.this, REQUEST_TIMER);
                dialog.show(manager, DIALOG_TIMER);
            }
        });
        notesBox.setTypeface(Typefaces.COOPER_HEWITT_MEDIUM);
        t.setTypeface(Typefaces.COOPER_HEWITT_MEDIUM);
        t.setText(s.getName());
        timerList = (ListView)v.findViewById(R.id.timer_list);
        ArrayList<Double> warnings = s.getWarnings();
        tla = new TimersListAdapter(getContext(), warnings);
        timerList.setAdapter(tla);
        notesView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), EditSectionNotesActivity.class);
                i.putExtra("section", s);
                startActivityForResult(i, 1);
            }
        });
        analyticsList = (ListView)v.findViewById(R.id.section_analytics_list);
        //ArrayList<AnalyticPair> dummy = new ArrayList<AnalyticPair>();
        //dummy.add(new AnalyticPair(new Date(2015, 11, 1), 6.683));
        //dummy.add(new AnalyticPair(new Date(2015, 10, 27), 6.5));
        //dummy.add(new AnalyticPair(new Date(2015, 10, 26), 7.6));
        //dummy.add(new AnalyticPair(new Date(2015, 10, 24), 8.1));
        sala = new SectionAnalyticListAdapter(getContext(), s.getPracticeRuns());
        analyticsList.setAdapter(sala);
        return v;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("MAIN", "got result");
        if (resultCode == Activity.RESULT_OK && requestCode == 1) {
            Log.d("MAIN", "got RESULT_OK");
            String notes = data.getStringExtra("newNotes");
            s.editNotes(notes);
            notesBox.setText(notes);
        } else if (resultCode == Activity.RESULT_OK) {
            Log.d("MAIN", "timer set");
            int m = data.getIntExtra("minutes", 0);
            int sec = data.getIntExtra("seconds", 0);
            double d = m + sec / 60.0;
            if (d > 0) {
                tla.addItem(d);
                tla.notifyDataSetChanged();
                //s.addWarning(d);
                //tla = new TimersListAdapter(getContext(), s.getWarnings());
                //timerList.setAdapter(tla);
            }
        }
    }

}
