package com.eclecticeverywhere.elo;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AnalyticPair implements Serializable {
    public Date date;
    public double time;
    public int peeks;
    public AnalyticPair(Date date, double time) {
        this.date = date;
        this.time = time;
    }

    public AnalyticPair(Date date, double time, int peeks) {
        this.date = date;
        this.time = time;
        this.peeks = peeks;
    }

    public String dateToString() {
        SimpleDateFormat format1 = new SimpleDateFormat("MM/dd");
        return format1.format(this.date);
    }
}