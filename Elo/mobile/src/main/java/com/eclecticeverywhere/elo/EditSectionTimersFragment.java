//package com.eclecticeverywhere.elo;
//
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//
///**
// * Created by Eric on 12/3/15.
// */
//public class EditSectionTimersFragment extends Fragment {
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//    }
//
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        View v = inflater.inflate(R.layout.fragment_edit_sections_timers, container, false);
//        return v;
//    }
//}

package com.eclecticeverywhere.elo;

import android.content.Intent;
import android.support.v4.app.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.wearable.DataMap;

import java.util.ArrayList;

/**
 * Created by Eric on 12/3/15.
 */
public class EditSectionTimersFragment extends Fragment {

    public final static int[] colorSeq = {R.color.section1, R.color.section2, R.color.section3, R.color.section4};
    private MultiSlider multiSlider;
    private ListView sectionsDisplay;
    private EditSectionTimerListAdapter sla;
    private TextView totalPresTime;
    private View v;
    private TextView sectionSlide;
    private ArrayList<Section> sectionsList;
    private int sectionNum = 3;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_edit_sections_timers, container, false);
        sectionSlide = (TextView) v.findViewById(R.id.section_slide);
//        final ArrayList<TextView> vals = new ArrayList<TextView>();
//        vals.add((TextView) v.findViewById(R.id.value1));
//        vals.add((TextView) v.findViewById(R.id.value2));
//        vals.add((TextView) v.findViewById(R.id.value3));
//        vals.add((TextView) v.findViewById(R.id.value4));
//        vals.add((TextView) v.findViewById(R.id.value5));
//        vals.add((TextView) v.findViewById(R.id.value6));
//        vals.add((TextView) v.findViewById(R.id.value7));

        multiSlider = (MultiSlider)v.findViewById(R.id.multiSlider);
        multiSlider.setOnThumbValueChangeListener(new MultiSlider.SimpleOnThumbValueChangeListener() {
            @Override
            public void onValueChanged(MultiSlider multiSlider, MultiSlider.Thumb thumb, int thumbIndex, int value) {
                if(thumbIndex==0){
                    sectionsList.get(thumbIndex).setTime(value);
                    sectionsList.get(thumbIndex + 1).setTime(multiSlider.getThumb(thumbIndex + 1).getValue()-value);
                }
                else if (thumbIndex==sectionsList.size()-2){
                    sectionsList.get(thumbIndex).setTime(value-multiSlider.getThumb(thumbIndex - 1).getValue());
                    sectionsList.get(thumbIndex + 1).setTime(30-value);
                }
                else{
                    sectionsList.get(thumbIndex).setTime(value-multiSlider.getThumb(thumbIndex -1).getValue());
                    sectionsList.get(thumbIndex + 1).setTime(multiSlider.getThumb(thumbIndex + 1).getValue()-value);
                }
                ((EditSectionsTabActivity)getActivity()).SavePreferences();
                Section[] initial = new Section[sectionsList.size()];
                initial = sectionsList.toArray(initial);
                setHeaderDisplay(initial);
                sla = new EditSectionTimerListAdapter(getActivity(), initial);
                sectionsDisplay.setAdapter(sla);
            }

        });


        // sections
        sectionsList = ((EditSectionsTabActivity)getActivity()).getSection();
        sectionsDisplay = (ListView)v.findViewById(R.id.sections_display);
        totalPresTime = (TextView)v.findViewById(R.id.total_time);
        totalPresTime.setTypeface(Typefaces.COOPER_HEWITT_MEDIUM);
        Section[] initial = new Section[sectionsList.size()];
        initial = sectionsList.toArray(initial);
        setHeaderDisplay(initial);
        sla = new EditSectionTimerListAdapter(getActivity(), initial);
        sectionsDisplay.setAdapter(sla);

        return v;
    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser)
        {
            sectionsList = ((EditSectionsTabActivity)getActivity()).getSection();
            if(sectionsList.size()>=4){
                for (int i = sectionsList.size();i>3;i--){
                    multiSlider.addThumb();
                    multiSlider.setVisibility(View.GONE);
                    multiSlider.setVisibility(View.VISIBLE);
                }
                sectionsList.get(1).setTime(3.0);
                Section[] initial = new Section[sectionsList.size()];
                initial = sectionsList.toArray(initial);
                setHeaderDisplay(initial);
                sla = new EditSectionTimerListAdapter(getActivity(), initial);
                sectionsDisplay.setAdapter(sla);
            }
        }
        else {  }
    }
    public void setHeaderDisplay(Section[] sections) {
        ArrayList<Integer> sizes = new ArrayList<Integer>();
        double totalTime = 0.0;
        int numSlides = 0;
        for (Section s : sections) {
            int size = s.getSectionEnd() - s.getSectionStart() + 1;
            sizes.add(size);
            numSlides += size;
            totalTime += s.getTimeAllowed();
        }
        int min = (int)totalTime;
        int sec = (int)((totalTime - min) * 60);
        totalPresTime.setText(String.format("%d:%d%d", min, sec / 10, sec % 10));
        LinearLayout view = (LinearLayout) v.findViewById(R.id.section_length_bar);
        for (int i = 0 ; i < sizes.size(); i++) {
            int pixels = (int)(sizes.get(i) / (double)numSlides * 600);
            TextView t = new TextView(getActivity());
            int color = colorSeq[i % colorSeq.length];
            t.setHeight(60);
            t.setWidth(pixels);
            t.setBackgroundColor(ContextCompat.getColor(getActivity(), color));
            view.addView(t);
        }


    }
}