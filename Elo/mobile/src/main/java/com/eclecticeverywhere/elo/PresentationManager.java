package com.eclecticeverywhere.elo;
import java.util.ArrayList;
import java.util.Date;

public class PresentationManager {

    private static PresentationManager current_manager;
    public ArrayList<Presentation> presentationList;
    public Presentation activePresentation = null;
    public Section activeSection = null;

    public static PresentationManager getInstance() {
        if (current_manager == null) {
            current_manager = new PresentationManager();
        }
        return current_manager;
    }

    private PresentationManager() {
        presentationList = new ArrayList<Presentation>();
        Presentation p = new Presentation("DESIGN 05 ELO PRESENTATION");
        p.setTime(6.0);
        p.totalSlides = 16;
        p.mCurrentSlide = 1;
        Section sec1 = new Section("Introduction", 1, 4);
        Section sec2 = new Section("Design Process", 5, 11);
        Section sec3 = new Section("Technical Challenges", 12, 16);
        sec1.setTime(1);
        sec2.setTime(3.0);
        sec3.setTime(2.0);
        sec1.addWarning(1.0);
        sec2.addWarning(2.0);
        sec3.addWarning(3.0);
        sec1.addPracticeRun(new AnalyticPair(new Date(2015, 10, 24), 1.5));
        sec1.addPracticeRun(new AnalyticPair(new Date(2015, 10, 26), 2.3));
        sec1.addPracticeRun(new AnalyticPair(new Date(2015, 10, 27), 3.1));
        sec1.addPracticeRun(new AnalyticPair(new Date(2015, 11, 1), 2.9));
        sec2.addPracticeRun(new AnalyticPair(new Date(2015, 11, 1), 2.5));
        sec3.addPracticeRun(new AnalyticPair(new Date(2015, 11, 1), 4.5));
        p.addSection(sec1);
        p.addSection(sec2);
        p.addSection(sec3);
        p.addPracticeRun(new AnalyticPair(new Date(2015, 11, 1), 5.8, 4));
        Presentation p2 = new Presentation("FINAL PRESENTATION");
        presentationList.add(p);
        presentationList.add(p2);

    }
}