package com.eclecticeverywhere.elo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by Eric on 12/3/15.
 */
public class AnalyticsFragment extends Fragment {

    RecyclerView recyclerView;
    ArrayList<RowItems> itemsList = new ArrayList<>();
    RecyclerViewAdapter adapter;
    JSONObject parent;
    View v;
    AnalyticPair latestRun;

    private Presentation p;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_analytics, container, false);
        p = PresentationManager.getInstance().activePresentation;
        TextView feedbackHeader = (TextView)v.findViewById(R.id.feedbackHeaderText);
        feedbackHeader.setTypeface(Typefaces.COOPER_HEWITT_MEDIUM);
        TextView timeHeader = (TextView)v.findViewById(R.id.timeTaken);
        timeHeader.setTypeface(Typefaces.COOPER_HEWITT_MEDIUM);
        TextView overHeader = (TextView)v.findViewById(R.id.wentOverText);
        overHeader.setTypeface(Typefaces.COOPER_HEWITT_MEDIUM);
        TextView peekHeader = (TextView)v.findViewById(R.id.peeks);
        peekHeader.setTypeface(Typefaces.COOPER_HEWITT_MEDIUM);
        TextView sectionHeader = (TextView)v.findViewById(R.id.sectionText);
        sectionHeader.setTypeface(Typefaces.COOPER_HEWITT_MEDIUM);
        TextView expectedHeader = (TextView)v.findViewById(R.id.expectedText);
        expectedHeader.setTypeface(Typefaces.COOPER_HEWITT_MEDIUM);
        TextView actualHeader = (TextView)v.findViewById(R.id.actualText);
        actualHeader.setTypeface(Typefaces.COOPER_HEWITT_MEDIUM);

        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        getData();
        adapter = new RecyclerViewAdapter(getActivity(), setDataTable());
        recyclerView.setAdapter(adapter);
        latestRun = p.getLatestRun();
        setTotalTime();
        setTimeOver();
        setPeekCount();

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        PresentationManager pm = PresentationManager.getInstance();
                        pm.activeSection = pm.activePresentation.getSections().get(position);
                        RowItems row = adapter.getItem(position);
                        Intent i = new Intent(getActivity(), AnalyticsSectionActivity.class);
                        i.putExtra("row", row);
                        startActivity(i);
                    }
                })
        );

        return v;
    }

    public void setTotalTime(){
        if (latestRun != null) {
            TextView totalTime = (TextView) v.findViewById(R.id.totalTime);
            totalTime.setTypeface(Typefaces.COOPER_HEWITT_MEDIUM);
            double time = latestRun.time;
            int min = (int)time;
            int sec = Math.round((float)(time - min)*60);
            totalTime.setText(String.format("%d:%d%d", min, sec / 10, sec % 10));
            //JSONArray dates = parent.getJSONArray("dates");
            //JSONArray current = dates.getJSONObject(1).getJSONArray("current");
            //totalTime.setText(current.getJSONObject(current.length() - 1).getString("time"));

        //} catch (JSONException e) {
          //  e.printStackTrace();
        }
    }

    public void setPeekCount(){
        if (latestRun != null) {
            TextView peeksCount = (TextView) v.findViewById(R.id.peeksCount);
            peeksCount.setTypeface(Typefaces.COOPER_HEWITT_MEDIUM);
            //JSONArray dates = parent.getJSONArray("dates");
            //peeksCount.setText(dates.getJSONObject(1).getString("peeks"));
            peeksCount.setText(String.format("%d", latestRun.peeks));
       // } catch (JSONException e) {
         //   e.printStackTrace();
        }
    }

    public void setTimeOver(){
        try {
            TextView timeOver = (TextView) v.findViewById(R.id.wentOver);
            timeOver.setTypeface(Typefaces.COOPER_HEWITT_MEDIUM);
            double actual = latestRun.time;
            double expected = p.getTimeAllowed();
            double diff = actual - expected;
            if (diff <= 0) {
                timeOver.setText("0:00");
            } else {
                int min = (int)diff;
                int sec = (int)((diff - min)*60.0);
                timeOver.setText(String.format("%d:%d%d", min, sec/10, sec%20 ));
            }

            // } catch (JSONException e) {
            //   e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public void getData() {
        parseJSONData();
        //Get the JSON object from the data
        parent = this.parseJSONData();
    }

    public ArrayList<RowItems> setDataTable() {
        ArrayList<RowItems> list = new ArrayList<RowItems>();
        try {
            //JSONArray dates = parent.getJSONArray("dates");
            //JSONArray expected = dates.getJSONObject(0).getJSONArray("expected");
            //JSONArray current = dates.getJSONObject(1).getJSONArray("current");
            ArrayList<Double> expected = p.getSectionTimes();
            ArrayList<Double> current = new ArrayList<Double>();
            ArrayList<Section> sections = p.getSections();
            for (int i = 0; i < expected.size(); i++) {
                AnalyticPair latest = sections.get(i).getLatestRun();
                if (latest == null) {
                    current.add(-1.0);
                } else {
                    current.add(latest.time);
                }

            }

           // for (int i = 0; i < expected.length(); i++) {
            for (int i = 0; i < expected.size(); i++) {
             //   JSONObject expectedItem = expected.getJSONObject(i);
               // JSONObject currentItem = current.getJSONObject(i);
                double expectedItem = expected.get(i);
                double currentItem = current.get(i);

                int minExp = (int)expectedItem;
                int secExp = (int)((expectedItem - minExp)*60.0);
                RowItems item = new RowItems();
                //item.setNumber(expectedItem.getString("section"));
                //item.setExpected(expectedItem.getString("time"));
                //item.setActual(currentItem.getString("time"));
                item.setNumber(p.getSections().get(i).getName());
                item.setExpected(String.format("%d:%d%d", minExp, secExp / 10, secExp % 10));
                if (currentItem < 0) {
                    item.setActual("0:00");
                } else {
                    int minCur = (int) currentItem;
                    int secCur = (int) ((currentItem - minCur) * 60.0);
                    item.setActual(String.format("%d:%d%d", minCur, secCur / 10, secCur % 10));
                }
                list.add(item);
            }
        //} catch (JSONException e) {
          //  e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return list;
    }

    //Method that will parse the JSON file and will return a JSONObject
    public JSONObject parseJSONData() {
        String JSONString = null;
        JSONObject JSONObject = null;
        try {

            //open the inputStream to the file
            InputStream inputStream = getActivity().getAssets().open("analytics.json");

            int sizeOfJSONFile = inputStream.available();

            //array that will store all the data
            byte[] bytes = new byte[sizeOfJSONFile];

            //reading data into the array from the file
            inputStream.read(bytes);

            //close the input stream
            inputStream.close();

            JSONString = new String(bytes, "UTF-8");
            JSONObject = new JSONObject(JSONString);

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        catch (JSONException x) {
            x.printStackTrace();
            return null;
        }
        return JSONObject;
    }
}