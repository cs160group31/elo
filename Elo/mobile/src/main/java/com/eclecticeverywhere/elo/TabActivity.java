package com.eclecticeverywhere.elo;

import android.media.Image;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.wearable.DataMap;

/**
 * Created by Eric on 12/4/15.
 */
public abstract class TabActivity extends AppCompatActivity
    implements ViewPager.OnPageChangeListener
{
    ViewPager mViewPager;
    Fragment[] mTabs;
    String[] mTabNames;
    TextView mToolbarTitle;
    ImageButton mPresentButton;
    TextView mTabTitleTextView;
    ImageButton mAddButton;

    protected abstract void initTabs(); // initializes mTabs and mTabNames

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    @Override
    public void onPageSelected(int position) {
        Log.i("TEST", "" + position);
        mTabTitleTextView.setText(mTabNames[position]);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab);
        Log.i("TEST", "oncreate");

        DataMap args = DataMap.fromBundle(
                getIntent().getBundleExtra(Constants.EXTRA_DATA_MAP));
        String presentationTitle = args.getString(Constants.TOOLBAR_TITLE_KEY);

        initTabs();

        mTabTitleTextView = (TextView) findViewById(R.id.tab_title_text_view);
        mTabTitleTextView.setTypeface(Typefaces.COOPER_HEWITT_MEDIUM);

        mAddButton = (ImageButton) findViewById(R.id.tab_add_image_button);

        mViewPager = (ViewPager) findViewById(R.id.activity_presentation_view_pager);
        mViewPager.setOnPageChangeListener(this);

        FragmentManager fragmentManager = getSupportFragmentManager();
        mViewPager.setAdapter(new FragmentPagerAdapter(fragmentManager) {
            @Override
            public Fragment getItem(int position) {
                return mTabs[position];
            }

            @Override
            public int getCount() {
                return mTabs.length;
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return mTabNames[position];
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        mToolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        mToolbarTitle.setText(presentationTitle);
        mToolbarTitle.setTypeface(Typefaces.COOPER_HEWITT_MEDIUM);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mPresentButton = (ImageButton) toolbar.findViewById(R.id.present_button);
        mPresentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.startActivitySlideUp(
                        TabActivity.this,
                        PresentationControllerActivity.class,
                        new DataMap());
            }
        });


        TabLayout tabLayout = (TabLayout) findViewById(R.id.presentation_tabs);
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.setTabTextColors(
                ContextCompat.getColor(this, R.color.white),
                ContextCompat.getColor(this, R.color.white)
        );
        tabLayout.setSelectedTabIndicatorColor(Constants.HIGHLIGHT_COLOR);
        tabLayout.setSelectedTabIndicatorHeight(Constants.HIGHLIGHT_HEIGHT);

        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(Typefaces.COOPER_HEWITT_SEMIBOLD);
                }
            }
        }
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }
}
