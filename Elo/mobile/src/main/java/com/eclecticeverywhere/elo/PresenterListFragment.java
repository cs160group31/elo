package com.eclecticeverywhere.elo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.wearable.DataMap;

/**
 * Created by Eric on 12/2/15.
 */
public class PresenterListFragment extends Fragment {
    private RecyclerView mPresentationRecyclerView;
    private PresentationAdapter mAdapter;
    private PresentationManager mPresentationManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresentationManager = PresentationManager.getInstance();


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_pres_list, container, false);

        mPresentationRecyclerView = (RecyclerView) v
                .findViewById(R.id.presentation_recycler_view);
        mPresentationRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mAdapter = new PresentationAdapter();
        mPresentationRecyclerView.setAdapter(mAdapter);

        return v;
    }

    private class PresentationHolder extends RecyclerView.ViewHolder
        implements View.OnClickListener
    {
        public TextView mTitleTextView;
        public TextView mDateTextView;
        private Presentation p;

        public PresentationHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            mTitleTextView = (TextView) itemView
                    .findViewById(R.id.pres_list_item_title);
            mDateTextView = (TextView) itemView
                    .findViewById(R.id.pres_list_item_date);
        }

        public void bindPresentation(Presentation p) {
            this.p = p;
            mTitleTextView.setText(p.getName());
            AnalyticPair ap = p.getLatestRun();
            if (ap == null) {
                mDateTextView.setText("N/A");
            } else {
                mDateTextView.setText(ap.dateToString());
            }

        }

        @Override
        public void onClick(View v) {
            Log.d("MAIN", "changing active");
            String presentationTitle = mTitleTextView.getText().toString();
            mPresentationManager.activePresentation = p;
            DataMap dataMap = new DataMap();
            dataMap.putString(Constants.TOOLBAR_TITLE_KEY, presentationTitle);
            Utils.startActivitySlideLeft(getActivity(), PresentationTabActivity.class, dataMap);
        }
    }

    private class PresentationAdapter extends RecyclerView.Adapter<PresentationHolder> {

        // Probably want to store some data here
        // and also initialize it using a constructor

        @Override
        public PresentationHolder onCreateViewHolder(ViewGroup parent, int i) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            // EDIT LIST ITEM VIEW in layout/list_item_presentaiton.xml
            View view = layoutInflater
                    .inflate(R.layout.list_item_presentation, parent, false);
            return new PresentationHolder(view);
        }

        @Override
        public void onBindViewHolder(PresentationHolder holder, int position) {
            Presentation p = mPresentationManager.presentationList.get(position);
            holder.bindPresentation(p);
        }

        @Override
        public int getItemCount() {
            return mPresentationManager.presentationList.size(); // CHANGE
        }
    }
}