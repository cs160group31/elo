package com.eclecticeverywhere.elo;

import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import java.util.List;

/**
 * Created by Eric on 10/11/15.
 */
public class MessageSender
{
    private GoogleApiClient mGoogleApiClient;

    public MessageSender(GoogleApiClient client) {
        Log.i("MESSAGE", "MessageSender created");
        mGoogleApiClient = client;
    }

    public void sendMessage(String path, String message) {
        Log.i("MESSAGE", String.format("Sending message: %s...", message));
        new SendMessageThread(mGoogleApiClient, path, message).start();
    }
}


/**
 * SendMessageThread subclass starts a new thread to send a message over the data layer.
 * see:  http://android-wear-docs.readthedocs.org/en/latest/sync.html
 */
class SendMessageThread extends Thread {
    String path;
    String message;
    GoogleApiClient mClient;

    final static String TAG = "MESSAGE";

    SendMessageThread(GoogleApiClient client, String p, String msg) {
        Log.i(TAG, "creating send message thread...");
        mClient = client;
        path = p;
        message = msg;
    }

    public void run() {
        Log.i(TAG, "preparing to send message...");
        NodeApi.GetConnectedNodesResult nodes =
                Wearable.NodeApi.getConnectedNodes(mClient).await();
        Log.i(TAG, "got connected nodes");
        List<Node> nodesList = nodes.getNodes();
        if (nodesList.size() != 0) {
            Node bestNode = null;
            for (Node node : nodesList) {
                bestNode = node;
                if (node.isNearby()) {
                    break;
                }
            }
            Log.i(TAG, "best node is: " + bestNode.getDisplayName());
            Log.i(TAG, "sending message...");
            MessageApi.SendMessageResult result = Wearable.MessageApi
                    .sendMessage(mClient, bestNode.getId(), path, message.getBytes()).await();
            if (result.getStatus().isSuccess()) {
                Log.i(TAG, String.format("Message: \"%s\" sent to node %s!",
                        message, bestNode.getDisplayName()));
            }
        } else {
            Log.i(TAG, "ERROR:  couldn't find any nodes");
        }
    }
}
