package com.eclecticeverywhere.elo;

import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;

/**
 * Created by Eric on 10/11/15.
 */
public class DataSender
{
    private final static String TAG = "DATASEND";

    private GoogleApiClient mGoogleApiClient;

    public DataSender(GoogleApiClient client) {
        Log.i(TAG, "DataSender created");
        mGoogleApiClient = client;
    }

    public void sendData(String path, DataMap dataMap) {
        new SendDataMapThread(mGoogleApiClient, path, dataMap).start();
    }
}


/**
 * SendDataMapThread is a helper Thread that sends a DataMap over the data-layer
 * see:  http://android-wear-docs.readthedocs.org/en/latest/data.html
 */
class SendDataMapThread extends Thread {
    String mPath;
    DataMap mDataMap;
    GoogleApiClient mClient;

    final static String TAG = "DATASEND";

    SendDataMapThread (GoogleApiClient client, String p, DataMap data) {
        Log.i(TAG, "SendDataMapThread created...");

        mClient = client;
        mPath = p;
        mDataMap = data;
    }

    public void run() {
        Log.i(TAG, "preparing to send data...");
        NodeApi.GetConnectedNodesResult nodes =
                Wearable.NodeApi.getConnectedNodes(mClient).await();
        Log.i(TAG, "got connected nodes");

        // Send only to first node (assuming one device is connected)
        if (nodes.getNodes().size() == 0) {
            Log.e(TAG, "ERROR:  couldn't find any nodes");
            return;
        }

        // Construct a DataRequest and send over the data layer
        PutDataMapRequest putDMR = PutDataMapRequest.create(mPath);
        putDMR.getDataMap().putAll(mDataMap);
        PutDataRequest request = putDMR.asPutDataRequest();
        Log.i(TAG, "sending data...");
        DataApi.DataItemResult result =
                Wearable.DataApi.putDataItem(mClient, request).await();
        if (result.getStatus().isSuccess()) {
            Log.i(TAG, "Successfully sent data!");
        } else {
            Log.e(TAG, "ERROR: failed to send DataMap");
        }
    }
}