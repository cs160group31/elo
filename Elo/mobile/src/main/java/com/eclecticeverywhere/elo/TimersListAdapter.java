package com.eclecticeverywhere.elo;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.wearable.DataMap;

import java.util.ArrayList;
import java.util.Arrays;

public class TimersListAdapter extends BaseAdapter {
    Context context;
    ArrayList<Double> data;
    private static LayoutInflater inflater = null;
    public TimersListAdapter(Context context, ArrayList<Double> data) {
        // TODO Auto-generated constructor stub
        this.context = context;
        this.data = data;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void addItem(double d) {
        data.add(d);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    @Override
    public Double getItem(int position) {
        // TODO Auto-generated method stub
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;
        final int pos = position;
        double d = data.get(position);
        if (vi == null)
            vi = inflater.inflate(R.layout.timerlist_row, parent, false);
        TextView timerMin = (TextView) vi.findViewById(R.id.timer_min);
        timerMin.setTypeface(Typefaces.COOPER_HEWITT_MEDIUM);
        timerMin.setText(String.format("%d", (int) d));
        TextView min = (TextView)vi.findViewById(R.id.mins);
        TextView secs = (TextView)vi.findViewById(R.id.secs);
        ImageView remove = (ImageView)vi.findViewById(R.id.timer_remove);
        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                data.remove(pos);
                notifyDataSetChanged();
            }
        });
        min.setTypeface(Typefaces.COOPER_HEWITT_MEDIUM);
        secs.setTypeface(Typefaces.COOPER_HEWITT_MEDIUM);
        TextView timerSec = (TextView)vi.findViewById(R.id.timer_sec);
        timerSec.setText(String.format("%d", (int)((d-(int)d)*60)));
        timerSec.setTypeface(Typefaces.COOPER_HEWITT_MEDIUM);
        return vi;
    }

}