package com.eclecticeverywhere.elo;

import android.support.v4.app.Fragment;
import android.os.Bundle;

public class PresenterListActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment() {
        return new PresenterListFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Typefaces.init(this);

        getSupportActionBar().hide();
    }
}
