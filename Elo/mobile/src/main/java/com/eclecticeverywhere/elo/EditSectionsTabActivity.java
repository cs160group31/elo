package com.eclecticeverywhere.elo;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Eric on 12/4/15.
 */
public class EditSectionsTabActivity extends TabActivity {
    private ArrayList<Section> sectionsList =  new ArrayList<>();
    @Override
    protected void initTabs() {
        mTabs = new Fragment[] {
                new EditSectionsFragment(),
                new EditSectionTimersFragment()
        };

        mTabNames = new String[] {
                "SLIDES",
                "TIMING"
        };
    }
    public ArrayList<Section> getSection()
    {
        return sectionsList;
    }
    public void setSection(ArrayList<Section> as)
    {
        sectionsList = as;
    }
    public void SavePreferences() {
        SharedPreferences sharedPreferences = getSharedPreferences("com.eclecticeverywhere.elo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        try {
            editor.putString("sectionsList", ObjectSerializer.serialize(sectionsList));
        } catch (Exception e) {

        }
        editor.commit();
    }
}