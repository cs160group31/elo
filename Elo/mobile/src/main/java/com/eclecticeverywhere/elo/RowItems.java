package com.eclecticeverywhere.elo;

import java.io.Serializable;

/**
 * Created by winstonkim on 12/6/15.
 */
public class RowItems implements Serializable {
    private String number;
    private String expected;
    private String actual;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getExpected() {
        return expected;
    }

    public void setExpected(String expected) {
        this.expected = expected;
    }

    public String getActual() {
        return actual;
    }

    public void setActual(String actual) {
        this.actual = actual;
    }
}