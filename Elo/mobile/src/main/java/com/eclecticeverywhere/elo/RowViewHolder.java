package com.eclecticeverywhere.elo;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * Created by winstonkim on 12/6/15.
 */
public class RowViewHolder extends RecyclerView.ViewHolder  {
    TextView number, expected, actual;

    public RowViewHolder(View view) {
        super(view);
        this.number = (TextView) view.findViewById(R.id.number);
        this.number.setTypeface(Typefaces.COOPER_HEWITT_MEDIUM);
        this.expected = (TextView) view.findViewById(R.id.expected);
        this.expected.setTypeface(Typefaces.COOPER_HEWITT_MEDIUM);
        this.actual = (TextView) view.findViewById(R.id.actual);
        this.actual.setTypeface(Typefaces.COOPER_HEWITT_MEDIUM);
    }
}