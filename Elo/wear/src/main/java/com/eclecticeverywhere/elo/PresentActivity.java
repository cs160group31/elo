package com.eclecticeverywhere.elo;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.google.android.gms.wearable.MessageEvent;
import com.google.gson.Gson;

import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Eric on 12/6/15.
 */
public class PresentActivity extends RootWatchActivity {
    protected CustomViewPager mViewPager;

//    private

    private Fragment[] mPages;

    protected Date mStartTime;

    protected int mTimeRemainingMins = 0;
    protected int mTimeRemainingSecs = 0;
    protected boolean mOvertime = false;


    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        Log.i(Constants.TAG, "new message received!asdfasfasfasdfadfasfd!!!!");
        String messagePath = messageEvent.getPath();
        super.onMessageReceived(messageEvent);

        if (RootWatchActivity.sSourceNodeId == null) {
            RootWatchActivity.sSourceNodeId = messageEvent.getSourceNodeId();
        }

        if (messagePath.equals(Constants.MESSAGE_DATA_PATH)) {
            final String json = new String(messageEvent.getData());
            Log.i("DATA", "data received!!");
            RootWatchActivity.setupPresentation(json);
        }
    }

    public void updateCurrentSection() {
        List<Section> sections = RootWatchActivity.sPresentation.getSections();
        for (Section section : sections) {
            int start = section.getSectionStart();
            int end = section.getSectionEnd();
            if (sCurrentSlide >= start && sCurrentSlide < end) {
                if (sCurrentSection != section) {
                    // pause old timer, resume new timer.

                    sCurrentSection = section;
                    ((PresentFragment) mPages[0]).updateUI();
                }
            }
        }
    }

    private void setupTimers() {
        mStartTime = new Date();

        Timer updateTimer = new Timer();
        updateTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                long millis = new Date().getTime() - mStartTime.getTime();
                long timeRemaining = sTotalPresentationTime - millis;
                long timeElapsed = sTotalPresentationTime - timeRemaining;
                sTotalTimeElapsed = (int) timeElapsed / (1000 * 60);
                mTimeRemainingMins = (int) timeRemaining / (1000 * 60);
                mTimeRemainingSecs = (int) (timeRemaining / (1000)) % 60;
                mOvertime = timeRemaining < 0;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((PresentFragment) mPages[0]).updateUI();
                    }
                });

            }
        }, 0, 1000);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pager);

        RootWatchActivity.sMessageSender.sendMessage(Constants.MESSAGE_PATH,
                Constants.MESSAGE_STARTED);

        mViewPager = (CustomViewPager) findViewById(R.id.activity_view_pager);
        mViewPager.setPagingEnabled(false);
        mPages = new Fragment[5];
        mPages[0] = new PresentFragment();
        mPages[1] = new NotesFragment();
        mPages[2] = new MoreInfoFragment();
        mPages[3] = new FinishConfirmFragment();
        mPages[4] = new FinishAnalyticsFragment();

        FragmentManager fragmentManager = getSupportFragmentManager();
        mViewPager.setAdapter(new FragmentPagerAdapter(fragmentManager) {
            @Override
            public Fragment getItem(int position) {
                return mPages[position];
            }

            @Override
            public int getCount() {
                return mPages.length;
            }
        });

        setupTimers();


    }

//

}
