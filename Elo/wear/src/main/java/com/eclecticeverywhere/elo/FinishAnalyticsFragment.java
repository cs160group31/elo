package com.eclecticeverywhere.elo;

import android.os.Bundle;
import android.provider.DocumentsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;

/**
 * Created by Eric on 12/6/15.
 */
public class FinishAnalyticsFragment extends Fragment {

    protected TextView mPeekTextView;
    protected TextView mTotalTimeTextView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_finish_analytics, container, false);

        TextView finishMessage = (TextView) v.findViewById(R.id.finish_message);
        finishMessage.setTypeface(Typefaces.COOPER_HEWITT_BOLD);
        TextView totalTimeLabel = (TextView) v.findViewById(R.id.total_time_label);
        totalTimeLabel.setTypeface(Typefaces.COOPER_HEWITT_BOLD);
        TextView peekLabel = (TextView) v.findViewById(R.id.peeks_label);
        peekLabel.setTypeface(Typefaces.COOPER_HEWITT_BOLD);

        mPeekTextView = (TextView) v.findViewById(R.id.total_peek_text_view);
        mPeekTextView.setTypeface(Typefaces.COOPER_HEWITT_BOLD);

        mTotalTimeTextView = (TextView) v.findViewById(R.id.total_time_text_view);
        mTotalTimeTextView.setTypeface(Typefaces.COOPER_HEWITT_BOLD);

        mTotalTimeTextView.setText("" + RootWatchActivity.sTotalTimeElapsed + " mins");
        mPeekTextView.setText("" + RootWatchActivity.sTotalPeeks + " peeks");

        Button seeAnalyticsButton = (Button) v.findViewById(R.id.see_analytics_button);
        seeAnalyticsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("PRESENT_FRAGMENT", "see analytics button pressed");
                LoadingActivity.sStarted = false;

                AnalyticsData data = new AnalyticsData(
                        RootWatchActivity.sTotalPeeks, RootWatchActivity.sTotalTimeElapsed);
                Gson gson = new Gson();
                String json = gson.toJson(data);

                RootWatchActivity.sMessageSender.sendMessage(
                        Constants.MESSAGE_ANALYTICS_PATH, json);
                // send over analytics
                getActivity().finish();
            }
        });

        return v;
    }

    public class AnalyticsData {
        int peekCount;
        int totalTime;

        AnalyticsData(int peeks, int time) {
            peekCount = peeks;
            totalTime = time;
        }
    }
}
