package com.eclecticeverywhere.elo;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

/**
 * Created by Eric on 10/18/15.
 */
public abstract class SingleFragmentActivity extends RootWatchActivity {
    protected abstract Fragment firstFragment();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);

        // only need to do once when first starting!
        Typefaces.init(this);

        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragment_container);

        if (fragment == null) {
            fragment = firstFragment();
            fm.beginTransaction()
                    .add(R.id.fragment_container, fragment)
                    .commit();
        }
    }

    protected void swapFragments(Fragment newFragment) {
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction()
                .replace(R.id.fragment_container, newFragment)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }

//    protected void pushFragment(Fragment newFragment) {
//        FragmentManager fm = getSupportFragmentManager();
//        fm.beginTransaction()
//                .add(R.id.fragment_container, newFragment)
//                .addToBackStack(null)
//                .commit();
//    }
}
