package com.eclecticeverywhere.elo;

import android.app.Activity;
import android.app.Presentation;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.WindowManager;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Wearable;
import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by Eric on 12/7/15.
 */
public class RootWatchActivity extends FragmentActivity implements
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks,
        MessageApi.MessageListener,
        DataApi.DataListener

{

    private final static String TAG = "TEST";
    private static GoogleApiClient sGoogleApiClient;
    protected static MessageSender sMessageSender;

    private static ArrayList<Activity>sActivities=new ArrayList<Activity>();

    protected static String sSourceNodeId;

    protected static com.eclecticeverywhere.elo.Presentation sPresentation;

    protected static boolean sClientConnected = false;
    protected int instances = 0;


    protected static int sTotalPresentationTime;
    protected static int sTotalSlides;
    protected static int sCurrentSlide;
    protected static Section sCurrentSection;
    protected static int sTotalTimeElapsed;
    protected static int sCurrentSectionIndex;
    protected static int sTotalPeeks;

    public static void finishAll() {
        for (Activity activity:sActivities)
            activity.finish();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "on resume");

        if (sClientConnected) {
            if (instances == 0) {
                Wearable.MessageApi.addListener(sGoogleApiClient, this);
                Wearable.DataApi.addListener(sGoogleApiClient, this);
                instances += 1;
            }
            Log.i(TAG, "" + instances);
//            sMessageSender.sendMessage(Constants.MESSAGE_PATH, Constants.MESSAGE_START);
        }
        Log.i(Constants.TAG, "" + instances);

    }

    public void onPause() {
        super.onPause();
        Log.i(TAG, "on pause");
        if (sClientConnected) {
            if (instances == 1) {
                Wearable.MessageApi.removeListener(sGoogleApiClient, this);
                Wearable.DataApi.removeListener(sGoogleApiClient, this);
                instances -= 1;
            }
            Log.i(TAG, "" + instances);
        }
        Log.i(Constants.TAG, "" + instances);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sActivities.add(this);

        Log.i(TAG, "oncreate");

        if (sGoogleApiClient == null) {
            initGoogleApiClient();
            // sMessageSender created in OnConnected()
        }

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    public static void setupPresentation(String json) {
        Gson gson = new Gson();
        sPresentation = gson.fromJson(json, com.eclecticeverywhere.elo.Presentation.class);
        sTotalPresentationTime =
                (int) RootWatchActivity.sPresentation.getTimeAllowed() * 60 * 1000; // in milliseconds
        sCurrentSlide =
                (int) RootWatchActivity.sPresentation.mCurrentSlide;
        sTotalSlides =
                (int) RootWatchActivity.sPresentation.totalSlides;
        sCurrentSection = RootWatchActivity.sPresentation.getSections().get(0);
        Log.i("DATA", "name: " + sCurrentSection.getName());
        Log.i("DATA", "time allowed: " + sTotalPresentationTime);
        Log.i("DATA", "current slide: " + sCurrentSlide);
        Log.i("DATA", "total slides: " + sTotalSlides);
        Log.i("DATA", "warnings: " + sPresentation.getWarnings());
        Log.i("DATA", "section times: " + sPresentation.getSectionTimes());
    }

    // OVERRIDE ME!
    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        if (sSourceNodeId == null) {
            sSourceNodeId = messageEvent.getSourceNodeId();
        }

        String messagePath = messageEvent.getPath();

        if (messagePath.equals(Constants.MESSAGE_DATA_PATH)) {
            final String json = new String(messageEvent.getData());
            Log.i("DATA", "data received!!");
            setupPresentation(json);
        }
        if (messagePath.equals(Constants.MESSAGE_PATH)) {
            final String message = new String(messageEvent.getData());
            if (message.equals(Constants.MESSAGE_STOP)) {
                Log.i(TAG, "received kill signal");
                finishAll();
            }
            Log.i(Constants.TAG, "message received (root watch activity)!!!!");
        }
    }

    private void initGoogleApiClient() {
        Log.i(TAG, "initGoogleApiClient()");
        sGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        sGoogleApiClient.connect();
    }

    /**
     * METHODS FOR IMPLEMENTING GoogleApiClient INTERFACES!
     **/
    @Override
    public void onConnected(Bundle connectionHint) {
        Log.i(Constants.TAG, "connection success!");
        sClientConnected = true;
        if (instances == 0) {
            Wearable.MessageApi.addListener(sGoogleApiClient, this);
            Wearable.DataApi.addListener(sGoogleApiClient, this);

            instances += 1;
        }
        Log.i(Constants.TAG, "" + instances);

        sMessageSender = new MessageSender(sGoogleApiClient);
//        Wearable.MessageApi.addListener(sGoogleApiClient, this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LoadingActivity.sStarted = false;
        sActivities.remove(this);
    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.i(Constants.TAG, "connection suspended.");
//        Wearable.MessageApi.removeListener(sGoogleApiClient, this);
        sClientConnected = false;
    }
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(Constants.TAG, "connection FAILED.");
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEventBuffer) {

    }
}
