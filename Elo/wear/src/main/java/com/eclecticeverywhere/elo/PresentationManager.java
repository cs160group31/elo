package com.eclecticeverywhere.elo;
import java.util.ArrayList;
import java.util.Date;

public class PresentationManager {

    private static PresentationManager current_manager;
    public ArrayList<Presentation> presentationList;
    public Presentation activePresentation = null;
    public Section activeSection = null;

    public static PresentationManager getInstance() {
        if (current_manager == null) {
            current_manager = new PresentationManager();
        }
        return current_manager;
    }

    private PresentationManager() {
        presentationList = new ArrayList<Presentation>();
        Presentation p = new Presentation("DESIGN 05 ELO PRESENTATION");
        p.setTime(24.0);
        Section sec1 = new Section("Introduction", 1, 10);
        Section sec2 = new Section("Task Analysis", 11, 15);
        Section sec3 = new Section("Design Process", 16, 25);
        sec1.setTime(6.0);
        sec2.setTime(7.0);
        sec3.setTime(11.0);
        sec1.addWarning(1.0);
        sec2.addWarning(2.0);
        sec3.addWarning(3.0);
        sec1.addPracticeRun(new AnalyticPair(new Date(2015, 10, 24), 8.1));
        sec1.addPracticeRun(new AnalyticPair(new Date(2015, 10, 26), 7.6));
        sec1.addPracticeRun(new AnalyticPair(new Date(2015, 10, 27), 6.5));
        sec1.addPracticeRun(new AnalyticPair(new Date(2015, 11, 1), 6.83));
        sec2.addPracticeRun(new AnalyticPair(new Date(2015, 11, 1), 7.0));
        sec3.addPracticeRun(new AnalyticPair(new Date(2015, 11, 1), 11.0));
        p.addSection(sec1);
        p.addSection(sec2);
        p.addSection(sec3);
        p.addPracticeRun(new AnalyticPair(new Date(2015, 11, 1), 26.1, 4));
        Presentation p2 = new Presentation("AYY LMAO");
        presentationList.add(p);
        presentationList.add(p2);

    }
}