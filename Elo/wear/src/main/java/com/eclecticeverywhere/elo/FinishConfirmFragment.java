package com.eclecticeverywhere.elo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Eric on 12/6/15.
 */
public class FinishConfirmFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_finish_confirm, container, false);

        TextView confirm_title = (TextView) v.findViewById(R.id.finish_confirm_message);
        confirm_title.setTypeface(Typefaces.COOPER_HEWITT_BOLD);

        Button yesButton = (Button) v.findViewById(R.id.yes_button);
        yesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("PRESENT_FRAGMENT", "yes button clicked");
                ((PresentActivity) getActivity()).mViewPager.setCurrentItem(4, false);
            }
        });

        Button noButton = (Button) v.findViewById(R.id.no_button);
        noButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("PRESENT_FRAGMENT", "no button clicked");
                ((PresentActivity) getActivity()).mViewPager.setCurrentItem(0, false);
            }
        });

        return v;
    }
}
