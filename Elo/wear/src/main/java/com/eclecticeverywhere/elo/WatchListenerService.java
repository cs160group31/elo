package com.eclecticeverywhere.elo;

import android.content.Intent;
import android.util.Log;

import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

/**
 * Created by Eric on 10/11/15.
 */
public class WatchListenerService extends WearableListenerService {
    private final static String TAG = "TEST";

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        Log.i(TAG, "new message receivedasdfafsafs!");
        String messagePath = messageEvent.getPath();

        if (messagePath.equals(Constants.MESSAGE_DATA_PATH)) {
            final String json = new String(messageEvent.getData());
            Log.i("DATA", "data received!!");
            RootWatchActivity.setupPresentation(json);
        }

        if (messagePath.equals(Constants.MESSAGE_PATH)) {
            final String message = new String(messageEvent.getData());
            if (message.equals(Constants.MESSAGE_START)) {
                // Start MainActivity
                if (!LoadingActivity.sStarted) {
                    LoadingActivity.sStarted = true;
                    Intent activity = new Intent(this, LoadingActivity.class);
                    activity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    Log.i(TAG, "Starting new activity");
                    startActivity(activity);
                }
            }
            Log.i(TAG, String
                    .format("new message received from path %s:\n\"%s\"", messagePath, message));
        } else {
            super.onMessageReceived(messageEvent);
        }
    }


}
