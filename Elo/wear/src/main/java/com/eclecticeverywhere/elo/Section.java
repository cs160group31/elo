package com.eclecticeverywhere.elo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class Section implements Serializable {
    private int sectionStart;
    private int sectionEnd;
    private String name;
    private double timeAllowed;
    private ArrayList<Double> warnings;
    private String notes;
    private ArrayList<AnalyticPair> practiceRuns;

    public Section (int start, int end) {
        sectionStart = start;
        sectionEnd = end;
        warnings = new ArrayList<Double>();
        practiceRuns = new ArrayList<AnalyticPair>();
    }

    public Section (String secName, int start, int end) {
        name = secName;
        sectionStart = start;
        sectionEnd = end;
        warnings = new ArrayList<Double>();
        practiceRuns = new ArrayList<AnalyticPair>();
    }

    public String getName() {
        return name;
    }

    public void setTime(double time) {
        timeAllowed = time;
    }

    public void addWarning(double warning) {
        warnings.add(warning);
    }

    public void removeWarning(double warning) {
        warnings.remove(warning);
    }

    public ArrayList<Double> getWarnings() {
        return warnings;
    }

    public void changeLimits(int start, int end) {
        sectionStart = start;
        sectionEnd = end;
    }

    public int getSectionStart() {
        return sectionStart;
    }

    public int getSectionEnd() {
        return sectionEnd;
    }

    public void setSectionStart(int start)
    {
        sectionStart = start;
    }
    public void setSectionEnd(int end)
    {
        sectionEnd = end;
    }

    public void editNotes(String note) {
        notes = note;
    }

    public String getNotes() {
        return notes;
    }

    public JSONObject serialize() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("start", sectionStart);
            obj.put("end", sectionEnd);
            obj.put("timeAllowed", timeAllowed);
            JSONArray warnArr = new JSONArray(warnings);
            obj.put("warnings", warnArr);
            obj.put("notes", notes);
        } catch (JSONException e) {

        }
        return obj;
    }

    public double getTimeAllowed() {
        return timeAllowed;
    }

    public void addPracticeRun(AnalyticPair ap) {
        practiceRuns.add(0, ap);
    }

    public ArrayList<AnalyticPair> getPracticeRuns() {
        return practiceRuns;
    }

    public AnalyticPair getLatestRun() {
        if (practiceRuns.isEmpty()) {
            return null;
        }
        return practiceRuns.get(0);
    }

}