package com.eclecticeverywhere.elo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Eric on 12/6/15.
 */
public class PresentFragment extends Fragment {

    private Button mBackSlideButton;
    private Button mForwardSlideButton;
    private Button mMoreInfoButton;
    private Button mNotesButton;

    private TextView mTitleTextView;
    private TextView mTimeRemainingLabel;
    private TextView mSlideTimeLabel;
    private TextView mTimeRemainingMins;
    private TextView mTimeRemainingSecs;
    private TextView mTimeSlideMins;
    private TextView mTimeSlideSecs;
    private TextView mCurrentSlideTextView;
    private TextView mTotalSlidesTextView;

    private PresentActivity mActivity;

    public void updateUI() {
        if (mActivity == null) {
            return;
        }
        int mins = (int) Math.abs(mActivity.mTimeRemainingMins);
        int secs = (int) Math.abs(mActivity.mTimeRemainingSecs);
        mTimeRemainingMins.setText(String.format("%02d", mins));
        mTimeRemainingSecs.setText(String.format("%02d", secs));
        if (mActivity.mOvertime) {
            mTimeRemainingLabel.setText("TIME OVER");
            mTimeRemainingMins.setTextColor(
                    mActivity.getResources().getColor(R.color.lava));
            mTimeSlideMins.setTextColor(
                    mActivity.getResources().getColor(R.color.lava));
            mCurrentSlideTextView.setTextColor(
                    mActivity.getResources().getColor(R.color.lava));
        }

        if (RootWatchActivity.sPresentation != null) {
            mActivity.updateCurrentSection();
            mCurrentSlideTextView.setText("" + mActivity.sCurrentSlide);
            mTotalSlidesTextView.setText("" + mActivity.sTotalSlides);
            mTitleTextView.setText(mActivity.sCurrentSection.getName());
        }


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_present, container, false);

        Typefaces.init(getActivity());


        mActivity = (PresentActivity) getActivity();

        mBackSlideButton = (Button) v.findViewById(R.id.left_slide_button);
        mForwardSlideButton = (Button) v.findViewById(R.id.right_slide_button);
        mMoreInfoButton = (Button) v.findViewById(R.id.more_info_button);
        mNotesButton = (Button) v.findViewById(R.id.notes_button);

        mTitleTextView = (TextView) v.findViewById(R.id.presentation_title);
        mTimeRemainingLabel = (TextView) v.findViewById(R.id.time_remaining_label);
        mSlideTimeLabel = (TextView) v.findViewById(R.id.slide_time_label);
        mTimeRemainingMins = (TextView) v.findViewById(R.id.time_remaining_mins);
        mTimeRemainingSecs = (TextView) v.findViewById(R.id.time_remaining_secs);
        mTimeSlideMins = (TextView) v.findViewById(R.id.slide_time_mins);
        mTimeSlideSecs = (TextView) v.findViewById(R.id.slide_time_secs);
        mCurrentSlideTextView = (TextView) v.findViewById(R.id.current_slide_text_view);
        mTotalSlidesTextView = (TextView) v.findViewById(R.id.total_slides_text_view);

        mTitleTextView.setTypeface(Typefaces.COOPER_HEWITT_SEMIBOLD);
        mTimeRemainingLabel.setTypeface(Typefaces.COOPER_HEWITT_BOLD);
        mSlideTimeLabel.setTypeface(Typefaces.COOPER_HEWITT_BOLD);
        mTimeRemainingMins.setTypeface(Typefaces.COOPER_HEWITT_HEAVY);
        mTimeRemainingSecs.setTypeface(Typefaces.COOPER_HEWITT_MEDIUM);
        mTimeSlideMins.setTypeface(Typefaces.COOPER_HEWITT_HEAVY);
        mTimeSlideSecs.setTypeface(Typefaces.COOPER_HEWITT_HEAVY);
        mCurrentSlideTextView.setTypeface(Typefaces.COOPER_HEWITT_HEAVY);
        mTotalSlidesTextView.setTypeface(Typefaces.COOPER_HEWITT_MEDIUM);

        mBackSlideButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("PRESENT_FRAGMENT", "back button pressed");
                mActivity.sMessageSender
                        .sendMessage(Constants.MESSAGE_PATH, Constants.MESSAGE_BACK);
                if (mActivity.sCurrentSlide > 1) {
                    mActivity.sCurrentSlide--;
                }
                updateUI();
            }
        });

        mForwardSlideButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("PRESENT_FRAGMENT", "forward button pressed");
                mActivity.sMessageSender
                        .sendMessage(Constants.MESSAGE_PATH, Constants.MESSAGE_FORWARD);
                if (mActivity.sCurrentSlide < mActivity.sTotalSlides) {
                    mActivity.sCurrentSlide++;
                }
                updateUI();
            }
        });

        mNotesButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // touch down code
                        Log.i("PRESENT_FRAGMENT", "notes button touchdown");
                        ((PresentActivity) getActivity()).mViewPager.setCurrentItem(1, false);
                        RootWatchActivity.sTotalPeeks += 1;
                        break;

                    case MotionEvent.ACTION_UP:
                        // touch up code
                        Log.i("PRESENT_FRAGMENT", "notes button touchup");
                        ((PresentActivity) getActivity()).mViewPager.setCurrentItem(0, false);
                        break;
                }
                return false;
            }
        });

        mMoreInfoButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // touch down code
                        Log.i("PRESENT_FRAGMENT", "more info button touchdown");
                        ((PresentActivity) getActivity()).mViewPager.setCurrentItem(2, false);
                        break;

                    case MotionEvent.ACTION_UP:
                        // touch up code
                        Log.i("PRESENT_FRAGMENT", "more info button touchup");
                        int y = (int) event.getRawY();
                        Log.i("PRESENT_FRAGMENT", "y is: " + y);
                        if (y > 190) {
                            Log.i("PRESENT_FRAGMENT", "CANCEL BUTTON!");
                            ((PresentActivity) getActivity()).mViewPager.setCurrentItem(3, false);
                        } else {
                            ((PresentActivity) getActivity()).mViewPager.setCurrentItem(0, false);
                        }

                        break;
                }
                return false;
            }
        });

        updateUI();

        return v;

    }
}
