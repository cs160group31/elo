package com.eclecticeverywhere.elo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Eric on 12/6/15.
 */
public class ReadyFragment extends Fragment {

    private Button mStartButton;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_ready, container, false);

        TextView readyMessage = (TextView) v.findViewById(R.id.ready_message);
        readyMessage.setTypeface(Typefaces.COOPER_HEWITT_BOLD);

        mStartButton = (Button) v.findViewById(R.id.start_button);
        mStartButton.setTypeface(Typefaces.COOPER_HEWITT_BOLD);
        mStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("READY_FRAGMENT", "start!");
                RootWatchActivity.sMessageSender.sendMessage(
                        Constants.MESSAGE_PATH, Constants.MESSAGE_STARTED);
                Intent intent = new Intent(getActivity(), PresentActivity.class);
                getActivity().finish();
                getActivity().startActivity(intent);
            }
        });



        return v;
    }
}
