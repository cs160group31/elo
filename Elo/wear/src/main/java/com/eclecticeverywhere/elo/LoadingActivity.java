package com.eclecticeverywhere.elo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.MessageEvent;
import com.google.gson.Gson;

/**
 * Created by Eric on 12/6/15.
 */
public class LoadingActivity extends RootWatchActivity

{

    protected CustomViewPager mViewPager;

    protected static boolean sStarted;

//    private

    private Fragment[] mPages;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pager);

        Typefaces.init(this);

        Log.i(Constants.TAG, "LoadingActivity oncreate");

        mViewPager = (CustomViewPager) findViewById(R.id.activity_view_pager);
        mViewPager.setPagingEnabled(false);
        mPages = new Fragment[3];
        mPages[0]= new WaitingFragment();
        mPages[1] = new LoadingFragment();
        mPages[2] = new ReadyFragment();

        FragmentManager fragmentManager = getSupportFragmentManager();
        mViewPager.setAdapter(new FragmentPagerAdapter(fragmentManager) {
            @Override
            public Fragment getItem(int position) {
                return mPages[position];
            }

            @Override
            public int getCount() {
                return mPages.length;
            }
        });

    }

//    @Override
//    protected Fragment firstFragment() {
//        return new WaitingFragment();
//    }
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//
//    }
//
    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        Log.i(Constants.TAG, "new message received!asdfasfasfasdfadfasfd!!!!");
        String messagePath = messageEvent.getPath();
        super.onMessageReceived(messageEvent);

        if (RootWatchActivity.sSourceNodeId == null) {
            RootWatchActivity.sSourceNodeId = messageEvent.getSourceNodeId();
        }

        if (messagePath.equals(Constants.MESSAGE_DATA_PATH)) {
            final String json = new String(messageEvent.getData());
            Log.i("DATA", "data received!!");
            RootWatchActivity.setupPresentation(json);


        } else if (messagePath.equals(Constants.MESSAGE_PATH)) {
            final String message = new String(messageEvent.getData());
            Log.i(Constants.TAG, "message: " + message);
            if (message.equals(Constants.MESSAGE_START)) {
                mViewPager.setCurrentItem(0, false);
            }
            if (message.equals(Constants.MESSAGE_CONNECTED)) {
                Log.i(Constants.TAG, "message connected received");
                mViewPager.setCurrentItem(1, false);
//                swapFragments(new LoadingFragment());
            } else if (message.equals(Constants.MESSAGE_LOADED)) {
                Log.i(Constants.TAG, "message loaded received");
                mViewPager.setCurrentItem(2, false);
//                swapFragments(new ReadyFragment());
            } else if (message.equals(Constants.MESSAGE_RELOADED)) {
                Log.i(Constants.TAG, "message loaded received");
                Log.i("READY_FRAGMENT", "start!");
                Intent intent = new Intent(this, PresentActivity.class);
                finish();
                startActivity(intent);
//                swapFragments(new ReadyFragment());
            } else if (message.equals(Constants.MESSAGE_STARTED)) {
                Log.i(Constants.TAG, "message loaded received");
                Log.i("READY_FRAGMENT", "start!");
                Intent intent = new Intent(this, PresentActivity.class);
                finish();
                startActivity(intent);
            }
        }
    }
//
//    @Override
//    public void onDataChanged(DataEventBuffer dataEventBuffer) {
////        Log.i(Constants.TAG, "new dataMap received!");
////        DataMap dataMap;
////        for (int i=0; i < dataEventBuffer.getCount(); i++) {
////            DataEvent event = dataEventBuffer.get(i);
////            // Check the data type
////            if (event.getType() == DataEvent.TYPE_CHANGED) {
////                // Check the data path
////                String path = event.getDataItem().getUri().getPath();
////                if (path.equals(ALERT_PATH)) {
////                    Log.i(Constants.TAG, "dataMap received from path: " + path);
////                    dataMap = DataMapItem.fromDataItem(event.getDataItem()).getDataMap();
////                    Log.i(Constants.TAG, "Success!  DataMap received on watch:\n " + dataMap);
////
////                    // Start MainActivity
//////                    Intent mainActivity = new Intent(this, MainActivity.class);
//////                    mainActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//////                    mainActivity.addCategory("alert");
//////                    mainActivity.putExtra("dataMap", dataMap.toBundle());
//////
//////                    Log.i(TAG, "Starting new activity");
//////                    startActivity(mainActivity);
////                }
////            }
//        }
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        sStarted = false;
    }
}
