package com.eclecticeverywhere.elo;

/**
 * Created by Eric on 12/7/15.
 */
public class Constants {

    public final static String MESSAGE_PATH = "/message";
    public final static String MESSAGE_DATA_PATH = "/message_data";
    public final static String MESSAGE_ANALYTICS_PATH = "/message_analytics";
    public final static String MESSAGE_START = "/message/start";
    public final static String MESSAGE_CONNECTED = "/message/connected";
    public final static String MESSAGE_LOADED = "/message/loaded";
    public final static String MESSAGE_FORWARD = "/message/forward";
    public final static String MESSAGE_BACK = "/message/back";
    public final static String MESSAGE_ANALYTICS = "/message/analytics";
    public final static String MESSAGE_STOP = "/message/stop";
    public final static String MESSAGE_RELOADED = "/message/reloaded";
    public final static String MESSAGE_STARTED = "/message/started";

    public final static String TAG = "WATCH";
}
