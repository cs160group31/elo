package com.eclecticeverywhere.elo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by Eric on 12/6/15.
 */
public class MoreInfoFragment extends Fragment {

    private Button mEndPresentationButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_more_info, container, false);

//        Typefaces.init(getActivity());
//
        return v;

    }
}
