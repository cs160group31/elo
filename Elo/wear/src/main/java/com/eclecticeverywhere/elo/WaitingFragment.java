package com.eclecticeverywhere.elo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by Eric on 12/6/15.
 */
public class WaitingFragment extends Fragment {

    private TextView mMessageTextView;
    private ProgressBar mProgressBar;
    private RelativeLayout mContainerLayout;
    private int i;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_waiting, container, false);
        mMessageTextView = (TextView) v.findViewById(R.id.waiting_message);
        mMessageTextView.setTypeface(Typefaces.COOPER_HEWITT_BOLD);
        return v;
    }
}
