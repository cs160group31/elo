package com.eclecticeverywhere.elo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Eric on 12/6/15.
 */
public class NotesFragment extends Fragment {

    private TextView mTitleTextView;
    private TextView mNotesTextView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_notes, container, false);

        Typefaces.init(getActivity());

        mTitleTextView = (TextView) v.findViewById(R.id.notes_title);
        mNotesTextView = (TextView) v.findViewById(R.id.notes_text_field);

        mTitleTextView.setTypeface(Typefaces.COOPER_HEWITT_HEAVY);
        mNotesTextView.setTypeface(Typefaces.COOPER_HEWITT_SEMIBOLD);

        return v;

    }
}
